"""
    gestion.views, un module pour SLM
    - rendu des pages web à l'aide de modèles

    Copyright (C) 2023-2024 Georges Khaznadar <georgesk@debian.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

from django.shortcuts import render, redirect, HttpResponse
from django.db.models import Q, Count
from django.views.decorators.csrf import ensure_csrf_cookie
import django.db.models.fields.related
from django.utils.safestring import mark_safe
from django.utils.translation import gettext_lazy as _
from django.utils.translation import gettext
from django.utils.translation import activate, get_language_info

from .views_common import render_common
# from .views_download import *
# from .views_stats import *
# from .odfviews import *
from .models import Eleves, Materiel, LivreSupplement, Jeton, Inventaire, \
    Courriers, Classes, les_modeles, modeles_ordre, ETAT_MATERIEL_CHOIX, \
    Parametres, Prets, Niveaux, NOCLASS, Disciplines, Filieres, Series, \
    TypeMateriel, Commandes, Personnels, Details, sans_accent
from .forms import Eleves_Form, Courriers_Form, donnees_utilisateur_Form, \
    Eleves_ancien_Form, Parametres_Form, Materiel_Form
from .decorators import assure_jeton_valide, i18n
from .utils import svg_histogram, svg_actions_datees, urlToQrcode
from manuels.settings import MEDIA_ROOT, MEDIA_URL

from datetime import date, datetime, timedelta
from io import BytesIO
import pyqrcode
import pathlib
from collections import OrderedDict
import json
import math
from pprint import pformat
import re
import os
from subprocess import run
from lxml import etree
import unidecode

import locale
try:
    locale.setlocale(locale.LC_TIME, "fr_FR.UTF-8")
except locale.Error:
    print("Error: Failed to consider the locale fr_FR.UTF-8")

DATE111 = date.fromisoformat('0001-01-01')

# Create your views here.

@ensure_csrf_cookie
@i18n
def index(request):
    return redirect("/apropos#apropos")

@i18n
def jsi18n(request):
    """
    renvoie un catalogue Javascript pour certaines traductions courtes,
    sans contexte
    """
    translations = {
        'Oui': gettext('Oui'),
        'Non': gettext('Non'),
        'OK': gettext('OK'),
        'Valider': gettext('Valider'),
        'Echappement': gettext('Échappement'),
        'Supprimer': gettext('Supprimer'),
        'ERREUR': gettext('ERREUR'),
        'erreurs': gettext("Des erreurs ont été détectées"),
        'date_erreur': gettext("Date mal formée"),
        'Attention': gettext('Attention'),
        'Bof': gettext('Quelque chose s\'est passé de travers.'),
        'Confirm': gettext("Veuillez confirmer"),
        'Attendre': gettext('Attendre ...'),
        'Neuf': gettext('Neuf'),
        'Bon': gettext('Bon'),
        'Autre': gettext('Autre'),
        "code_inconnu": gettext("code inconnu ?"),
    }
    return HttpResponse(
        "const I18N = " + json.dumps(translations),
        content_type="application/javascript"
    )

@ensure_csrf_cookie
@i18n
def parametres(request):
    p = Parametres.objects.all().order_by(
        '-AnneeScolaire')
    return render_common(request, 'gestion/parametres.html',
                         _("Paramètres"),{
         "aide": "parametres",
         "param": p[0] if p else None,
     })

@ensure_csrf_cookie
@i18n
def apropos(request):
      return render_common(
          request, 'gestion/apropos.html', _("À propos"), pour_tous = True)
    
@ensure_csrf_cookie
@i18n
def assistance(request):
      return render_common(request, 'gestion/assistance.html',
                           _("Assistance"), {
          "aide": "assistance",
      })
    
@ensure_csrf_cookie
@i18n
def achats_inventaire(request):
      return render_common(request, 'gestion/achats_inventaire.html',
                           _("Achats/inventaire"),{
                               "aide": "achats_inventaire",
                           })
    
@ensure_csrf_cookie
@i18n
def prets(request):
    """
    Page de gestion des livres prêtés à un élève ;
    Un paramètre peut être posté, pour pré-remplir le champ de nomprenom
    de l'élève
    """
    nomprenom = request.POST.get("nomprenom","")
    return render_common(request, 'gestion/prets.html',
                         _("Livres prêtés, à ..."),{
        "nomprenom": nomprenom,
        "aide": "prets",
    })

@ensure_csrf_cookie
@i18n
def stats(request):
    """
    Affiche diverses statistiques
    """
    ladate = date.today()
    classes = list(Classes.classes_importantes())
    for c in classes:
        c.membres = len(Eleves.avec_prets_en_cours(classe=c.libelle))
        c.membres_pc = round(100 * c.membres / c.compte)
    manuel_catalogue = Materiel.objects.all().order_by("titre")

    class GlobalLivre:
        """
        données globales concernant des livres d'un même niveau/filière
        """
        def __init__(self):
            self.volume = 0
            self.masse = 0
            self.titres = set()
            
        def add(self, n, manuel):
            """
            ajoute les caractéristiques de n manuels (identiques)
            """
            self.volume += n * manuel.m3
            # densité du papier de l'ordre de 0,9
            self.masse += n * manuel.m3 * 900
            self.titres.add(manuel.titre)

        @property
        def les_titres(self):
            return ", ".join(sorted(list(self.titres)))
        
        def __str__(self):
            return _("""\
Valeurs globales : {volume} m³, {masse} kg, {les_titres}""").\
format(**self.__dict__)
        
    volumes = {}
    total = GlobalLivre()
    lesniveaux = set(m.niveau for m in manuel_catalogue)
    lesfilieres = set(m.filiere for m in manuel_catalogue)
    for niv in lesniveaux:
        for fil in lesfilieres:
            key = f"{niv.abrege} + {fil.nom}"
            manuels = Materiel.objects.filter(niveau = niv, filiere = fil)
            volumes[key] = GlobalLivre()
            for m in manuels:
                n = len(Inventaire.objects.filter(materiel = m))
                volumes[key].add(n, m)
                total.add(n,m)
    membres = Eleves.avec_prets_en_cours()
    livres_par_eleves = [e.prets_en_cours for e in membres]
    histogramme_lpe = svg_histogram(
        livres_par_eleves,
        _("Répartition des nombres de livres pour chaque élève"),
        _("livres"),
        bins = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16],
    )
    graphique_prets = None
    graphique_retours = None
    pr = Prets.objects.all()
    if pr:
        graphique_prets = svg_actions_datees(
            (p.date_pret for p in pr), _("date de prêt"))
        graphique_retours = svg_actions_datees(
            (p.date_retour for p in pr.filter(
                ~Q(date_retour = DATE111))),
            _("date de retour")
        )
    
    return render_common(request,'gestion/stats.html', "Statistiques ...", {
        "ladate": ladate,
        "classes": classes,
        "manuel_catalogue": manuel_catalogue,
        "volumes": volumes,
        "total": total,
        "nb_membres": len(membres),
        "histogramme_lpe": mark_safe(histogramme_lpe),
        "graphique_prets": mark_safe(graphique_prets),
        "graphique_retours": mark_safe(graphique_retours),
    })

@ensure_csrf_cookie
@i18n
def inventaire(request):
    """
    Implémente la page d'inventaire
    """
    niveaux = Niveaux.objects.filter(
        rang__gte = "E", rang__lte = "H").order_by("rang")
    categories = Classes.categories()
    return render_common(request,'gestion/inventaire.html',
                         _("Inventaire"),{
        "aide": "achats_inventaire#inventaire",
        "categories": categories,
        "niveaux": niveaux,
        "classes": [c.libelle for c in Classes.classes_importantes()],
    })

@ensure_csrf_cookie
@i18n
def constantes(request):
    constantes = Parametres.objects.all().order_by("AnneeScolaire")
    messages = Courriers.objects.all().order_by("N")
    p = constantes[len(constantes) - 1] if constantes else None
    classes = {c.libelle: c.compte \
               for c in Classes.classes_importantes()}
    return render_common(
        request, 'gestion/constantes.html',
        _("Constantes de l'établissement"),
        {
            "aide": "parametres#constantes",
            "constantes": constantes,
            "messages": messages,
            "param": p,
            "classes": classes,
            "total": sum(classes.values()),
        })

@ensure_csrf_cookie
@i18n
def constantes_modif(request):
    """
    Permet de voir et d'éditer la description de l'établissement
    """
    numero = request.POST.get("numero","")
    clonage = request.POST.get("clonage","")
    saved = False
    # l'instance à modifier sera définie à deux conditions : que le
    # paramètre numero ne soit pas vide, et que ce numéro identifie bien
    # un enregistrement existant.
    instance = Parametres.objects.filter(pk = int(numero)) if numero \
        else None
    instance = instance[0] if instance else None
    # si le paramètre modif_db n'est pas vide, alors le formulaire
    # est repris depuis les valeurs renseignées dans la page web
    # cependant que la clé primaire viendra toujours de l'instance
    # (que celle-ci soit définie ou non) ; si l'instance est indéfinie
    # ça provoquera la création d'un nouvel enregistrement
    form = Parametres_Form(
        request.POST if request.POST.get("modif_db", "") else None,
        request.FILES if request.POST.get("modif_db", "") else None,
        instance=instance)
    # test de validité avant d'enregistrer
    if form.is_valid():
        form.save()
        saved = True
    # affichage de l'interface web
    return render_common(
        request,'gestion/constantes_modif.html',
        _("Constantes de l'établissement"),
        {
            "numero": numero,
            "clonage": clonage,
            "form": form,
            "saved": saved,
        })
    
@ensure_csrf_cookie
@i18n
def catalogue(request):
    manuels = list(Materiel.objects.all().order_by("-Annee_Depot_Legal", "titre"))
    for m in manuels:
        # on enrichit chaque instance de Manuel de données à afficher en html
        # split_categories forme une liste de catégories
        m.split_categories = m.categories.split(", ")
        # split_options insère des <br> dans un nom d'option et
        # modifie MATHEMATIQUES (mot trop long)
        m.split_option = m.lib_option.replace(".&", ". &").\
            replace(" & ", " & ").replace(" ","<br>").\
            replace("-","<br>-").replace("S.P", "S·P").\
            replace(".", ".<br>").replace("MATHEMATIQUES", "MATHS").\
            replace("<br><br>", "<br>")
    return render_common(
        request, 'gestion/catalogue.html',
        _("Catalogue de la Coopérative"),
        {
            "aide": "achats_inventaire#catalogue",
            "manuels": manuels,
        })

@ensure_csrf_cookie
@i18n
def catalogue_modif(request):
    """
    Permet de voir et d'éditer les entrées du catalogue
    """
    numero = request.POST.get("numero","")
    clonage = request.POST.get("clonage","")
    saved = False
    # l'instance à modifier sera définie à deux conditions : que le
    # paramètre numero ne soit pas vide, et que ce numéro identifie bien
    # un enregistrement existant.
    instance = Materiel.objects.filter(pk = int(numero)) if numero \
        else None
    instance = instance[0] if instance else None
    # si le paramètre modif_db n'est pas vide, alors le formulaire
    # est repris depuis les valeurs renseignées dans la page web
    # cependant que la clé primaire viendra toujours de l'instance
    # (que celle-ci soit définie ou non) ; si l'instance est indéfinie
    # ça provoquera la création d'un nouvel enregistrement
    form = Materiel_Form(
        request.POST if request.POST.get("modif_db", "") else None,
        instance=instance)
    # test de validité avant d'enregistrer
    if form.is_valid():
        form.save()
        saved = True
    # affichage de l'interface web
    categories = json.dumps(Classes.categories())
    options = set([e.Lib_Mat_Enseignee_1 for e in Eleves.objects.all()]) | \
         set([e.Lib_Mat_Enseignee_2 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_3 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_4 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_5 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_6 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_7 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_8 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_9 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_10 for e in Eleves.objects.all()])
    options = sorted([ o for o in options if o])
    return render_common(
        request,'gestion/catalogue_modif.html',
        _("Gestion d'un des items du catalogue"),
        {
            "numero": numero,
            "categories": categories,
            "options": options,
            "clonage": clonage,
            "form": form,
            "saved": saved,
        })
    
@ensure_csrf_cookie
@i18n
def purger_prets(request):
    """
    Permet de purger les prêts quand le livre est revenu
    Considère les paramètres postés date1 et date2 : intervalle de temps
    """
    date1 = request.POST.get("date1", "")
    date2 = request.POST.get("date2", "")
    prets_rendus = Prets.objects.filter(
        date_retour__gt = DATE111)
    if date1 :
        prets_rendus = prets_rendus.filter(
            date_pret__gt = date1)
    if date2 :
        prets_rendus = prets_rendus.filter(
            date_retour__lt = date2)
    return render_common(
        request,'gestion/purger_prets.html',
        _("Purger les prêts anciens"),
        {
            "aide": "parametres#purger",
            "prets_rendus" : prets_rendus,
            "date1" : date1,
            "date2" : date2,
        })

@ensure_csrf_cookie
@i18n
def siecle(request):
    """
    Import d'un fichier siecle, éventuellement POSTé (param fichier_siecle)
    """
    liste_doublons_SLM = {}
    liste_eleves_SLM = Eleves.objects.all()
    liste_doublons_SLM = {
        e.sans_accent: e for e in liste_eleves_SLM \
                if len(e.homonymes(liste_eleves_SLM)) > 1
    }
    return render_common(
        request,'gestion/siecle.html',
        _("Importation depuis la base de données SIÈCLE"),
        {
            "aide": "parametres#siecle",
            "liste_doublons_SLM": liste_doublons_SLM,
        })
    
@ensure_csrf_cookie
@i18n
def eleves(request):
    """
    Affiche la page de gestion des enregistrements des élèves ;
    deux sortes de paramètres peuvent être reçus (postés), certains feront
    voir le formulaire relatif à un élève, d'autres feront voir le
    formulaire relatif à une classe
    """
    numero = request.POST.get("numero","")
    nomprenom = request.POST.get("nomprenom","")
    classe = request.POST.get("choix_classe","")
    saved = False
    if classe:
        eleves = Eleves.objects.filter(
            classe__libelle__iexact = classe).order_by('NomPrenomEleve')
        return render(
            request,'gestion/eleves_de_classe.html',
            {
                "classe": classe,
                "eleves": eleves,
            })
        
    if nomprenom:
        found = Eleves.objects.filter(NomPrenomEleve = nomprenom)
        numero = found[0].id if found else None
    else:
        # l'instance à modifier sera définie à deux conditions : que le
        # paramètre numero ne soit pas vide, et que ce numéro identifie bien
        # un enregistrement existant.
        found = Eleves.objects.filter(id = int(numero))\
            if numero  else None
    instance = found[0] if found else None
    # si le paramètre modif_db n'est pas vide, alors le formulaire
    # est repris depuis les valeurs renseignées dans la page web
    # cependant que la clé primaire viendra toujours de l'instance
    # (que celle-ci soit définie ou non) ; si l'instance est indéfinie
    # ça provoquera la création d'un nouvel enregistrement
    post_data = request.POST if request.POST.get("modif_db", "") else None
    # on choisit le formulaire selon no_caution
    form_class = Eleves_ancien_Form if \
        instance and instance.sorte_de_caution == "ancienne" \
        else Eleves_Form
    form = form_class(post_data, instance=instance)
    # test de validité avant d'enregistrer
    if form.is_valid():
        # avant d'enregistrer on retire le drapeau "a_verifier" qui pourrait
        # rester d'une insription provisoire, ainsi que
        # l'autorisation provisoire d'inscription
        eleve = form.save(commit=False)
        eleve.a_verifier=False
        eleve.autorisation_provisoire_inscription = None
        eleve.save()
        saved = True
        # mise à jour des effectifs des classes
        if found:
            found[0].classe.recompte()
        eleve.classe.recompte()
    # affichage de l'interface web
    result = None
    if nomprenom:
        constantes = Parametres.objects.all().order_by("AnneeScolaire")
        param = constantes[len(constantes) - 1]
        ee = Eleves.objects.filter(NomPrenomEleve = nomprenom)
        besoin_carte = ee[0].peut_recevoir_carte_membre if ee else False
        sans_caution = ee[0].sans_caution if ee else False
        caution_rendue = ee[0].caution_rendue if ee else False
        no_caution = ee[0].no_caution if ee else 0
        result= render(
            request,'gestion/eleve_modif.html',
            {
                "numero": numero,
                "nomprenom": nomprenom,
                "besoin_carte": besoin_carte,
                "sans_caution": sans_caution,
                "caution_rendue": caution_rendue,
                "no_caution": no_caution,
                "form": form,
                "param": param,
                "valide": form["Nom_de_famille"].value() is not None and \
                          form["Prenom"].value()
            })
    else:
        # aucun éléve, aucune classe ne sont demandés
        result= render_common(
            request,'gestion/eleves.html',
            _("Gestion de la base Élèves"),
            {
                "aide": "parametres#base_eleves",
                "saved": saved,
            })
    return result
            
@ensure_csrf_cookie
@i18n
def cartes(request):
    """
    Affiche la page de gestion des cartes de membres ;
    Le POST peut apporter les paramètres
    - liste_classes : une liste de classes selectionnées
    - filtrage: une liste de filtre autorisant des classes sélectionnables
    """
    filtrage = [f for f in request.POST.get("filtrage", "").split(",") if f]
    if len(filtrage) == 0:
        filtrage = ["2D", "1G", "1STMG", "TG", "TMC", "TRH"]
    Qexpr= Q(libelle__startswith=filtrage[0])
    for f in filtrage[1:]:
        Qexpr= Qexpr | Q(libelle__startswith=f)
    classes = Classes.objects.filter(compte__gt=0).filter(Qexpr)
    result= render_common(
        request,'gestion/cartes.html',
        _("Gestion des cartes de membre"),
        {
            "aide": "parametres#cartes_membre",
            "classes": classes,
            "filtrage": ",". join(filtrage),
        })
    return result
            
    
@ensure_csrf_cookie
@i18n
def bon_de_commande(request):
    return render_common(
        request,'gestion/bon_de_commande.html',
        _("Bons de commande"),
        {
        })
    
@ensure_csrf_cookie
@i18n
def tarif(request):
    return render_common(
        request,'gestion/tarif.html',
        _("Définition des tarifs"),
        {
        })
    
@ensure_csrf_cookie
@i18n
def codes_barres(request):
    """
    Gestion de codes-barres pour un manuel du catalogue.
    La clé primaire de ce manuel est dans le paramètre posté "numero"
    """
    numero = int(request.POST.get("numero", 0))
    manuel = None
    if numero:
        # on repère le manuel dans le catalogue
        manuel = Materiel.objects.get(id = numero)
        # on rassemble les manuels de ce titre déjà inventoriés
        items = Inventaire.objects.filter(
            materiel=numero).order_by("Date_Achat_Materiel")
        manuels_inventories = items.values("Date_Achat_Materiel").annotate(
            Count("id"))
        etats = [e[1] for e in ETAT_MATERIEL_CHOIX]
    return render_common(
        request,'gestion/codes_barres.html',
        _("Inventaire des manuels / Impression des étiquettes à codes-barres"),
        {
            "aide": "achats_inventaire#ajout_codes",
            "manuel": manuel,
            "manuels_inventories": manuels_inventories,
            "etats": etats,
        })
    
@ensure_csrf_cookie
@i18n
def codes_barres_print(request):
    """
    Impression de codes-barres pour un manuel du catalogue.
    Si le POST contient un paramètre "nombre", c'est pour créer
    de nouveaux manuels à l'inventaire, auquel cas il y a un autre
    paramètres "etat", clé de l'état (Neuf, Bon, etc.) ;
    s'il contient un paramètre "dejavu-???", c'est qu'on veut revoir
    des codes-barres existants ; dans tous les cas il y a un paramètre
    "id_catalogue".
    Le paramètre "tabou" permet de maintenir une liste de positions
    où il ne faut pas imprimer
    """
    nombre = int(request.POST.get("nombre", 0))
    etat = int(request.POST.get("etat", 0))
    id_catalogue = request.POST.get("id_catalogue","")
    tabou = request.POST.get("tabou","[]")
    date_dejavu = [request.POST[k] for k in request.POST \
              if k.startswith("dejavu-")]
    try:
        date_dejavu = date.fromisoformat(date_dejavu[0])
    except (ValueError, IndexError):
        date_dejavu = ""
    codes = []
    if date_dejavu:
        # on a choisi de revoir des manuels déjà dans l'inventaire
        codes = [m.id for \
                 m in Inventaire.objects.filter(
                     Date_Achat_Materiel = date_dejavu,
                     materiel = id_catalogue)]
    elif nombre:
        # on va créer nombre de manuels nouveaux et les inventorier
        # à la date du jour
        date_dejavu = datetime.now()
        materiel_catalogue = Materiel.objects.get(
            id = id_catalogue)
        for n in range(nombre):
            m = Inventaire(
                Date_Achat_Materiel = date_dejavu,
                materiel = materiel_catalogue,
                NumeroDeSerie = "",
                Perdu = 0,
                Etat_Materiel = int(etat),
                a_imprimer = 1,
            )
            m.save()
            codes.append(m.id)
    manuel = Materiel.objects.get(id = id_catalogue)
    return render_common(
        request,'gestion/codes_barres_print.html',
        _("Impression de codes-barres"),
        {
            "aide": "achats_inventaire#impression_codes",
            "manuel": manuel,
            "nombre": nombre,
            "date": date_dejavu,
            "codes": codes,
            "tabou": tabou,
        })

@ensure_csrf_cookie
@i18n
def cartes_membres_print(request):
    """
    Impression de cartes de membres.
    Le POST contient plusieurs paramètres :
      - id_eleves : une liste d'identifiants d'élèves à qui faire une carte
      - classes_chosen : une liste de classes
      - max_nb    : le nombre max de cartes qu'on peut imprimer sur la
                    première page donnée à l'imprimante, au cas où celle-ci
                    n'a pas toutes les positions imprimables.
    au cas où le POST ne contient qu'un paramètre nomprenom, on reconstitue
    tout de suite id_eleves (pour un seul élève), classes_chosen et
    max_nb
    """
    nomprenom = request.POST.get("nomprenom", "")
    max_nb = int(request.POST.get("max_nb","6")) # 0 c'est "pas de limite"
    if nomprenom :
        classes = []
        id_eleves = [Eleves.objects.get(NomPrenomEleve = nomprenom).id]
    else:
        id_eleves = json.loads(request.POST.get("id_eleves","[]"))
        classes = [ c.strip() for c in
                    json.loads(request.POST.get("classes_chosen","[]"))]
    if max_nb < 2:
        max_nb=2
    if max_nb > 8:
        max_nb=6
    eleves = []
    if classes:
        for c in classes:
            eleves += [e  for e in Eleves.objects.filter(Lib_Structure = c) \
                       if e.peut_recevoir_carte_membre]
    elif nomprenom:
        eleves = list(Eleves.objects.filter(NomPrenomEleve = nomprenom))
    # on prépare une prévisualisation... il s'agit de
    # rectangles de 105 × 74.25 px dans un canevas SVG de 210 × 297 px
    # en 4 lignes de deux cartes.
    rects = []
    nomsprenoms = [e.NomPrenomEleve for e in eleves][:max_nb] if max_nb \
        else [e.NomPrenomEleve for e in eleves][:6]
    # attention, si la feuille à imprimer est incomplète, et que
    # les nombre de cartes est plus petit que le nombre d'emplacements
    # disponibles, on imprime à la fin des cartons détachables !
    # c'est à dire, pour ici, on insère des textes vides
    if max_nb and len(nomsprenoms) < max_nb:
        nomsprenoms = [""] * (max_nb-len(nomsprenoms)) + nomsprenoms
    for i in range(3): # il y a 3 × 2 cartes à imprimer dans une feuille A4
        for j in range(2):
            if max_nb and 2*i+j >= max_nb:
                continue # ne pas chercher à mettre un nomprenom absent !
            rects.append(
                """\
                <rect x="{x}" y="{y}" width="105" height="99"
                style="fill:rgb(120,255,120);stroke-width:1;stroke:rgb(0,0,0)"
                />
                <text x="{x1}" y="{y1}" font-size="5px">
                {nomprenom}
                </text>            
                """.format(
                    x=j*210/2, y=i*297/3,
                    x1=20+j*210/2, y1=40+i*297/3,
                    nomprenom = nomsprenoms[2*i+j]
                )
            )
    # nombres de cartes par page
    cartes=[]
    a_imprimer = len(eleves)
    i = 1
    while a_imprimer:
        if max_nb and i==1:
            n = min(max_nb, a_imprimer)
        else:
            n = min (6, a_imprimer)
        cartes.append({"page": i, "imprime": n})
        a_imprimer -= n
        i+=1
    if len(cartes) > 8:
        cartes = cartes[:4] + [{"page": "...", "imprime": "..."}] + cartes[-4:]
    id_eleves = json.dumps([e.pk for e in eleves])
    return render_common(
        request,'gestion/cartes_membres_print.html',
        _("Impression de cartes de membres"),
        {
            "aide": "parametres#cartes_membre_selection",
            "classes" : json.dumps(classes),
            "nomprenom" : nomprenom,
            "id_eleves": id_eleves,
            "len_eleves": len(eleves),
            "rects": rects,
            "max_nb": max_nb,
            "cartes": cartes,
            "derniere": cartes[-1]["page"],
        })

@i18n
def cartes_membres_print_verso(request):
    """
    Impression de versos de cartes de membres.
    Le POST contient les paramètres :
      - nb : le nombre de pages qu'on veut imprimer, une par défaut
    """
    nb = int(request.POST.get("nb", "1"))
    return render_common(
        request,'gestion/cartes_membres_print_verso.html',
        _("Impression des versos de cartes de membre"),
        {
            "aide": "parametres#cartes_verso",
            "nb": nb,
            "cartes": 6 * nb,
        })

@ensure_csrf_cookie
@i18n
def retours(request):
    """
    Page de retour des livres
    les paramètres du POST sont éventuellement nomprenom et codelivre.
    """
    nomprenom = request.POST.get("nomprenom", "")
    livre = request.POST.get("livre", "")
    return render_common(
        request,'gestion/retours.html',
        _("Gestion des retours de manuels"),
        {
            "aide": "retours",
            "nomprenom": nomprenom,
            "livre": livre,
        })

@ensure_csrf_cookie
@i18n
def sauvegarde(request):

    def sauvegarde_date(path):
        """
        renvoie la date et l'heure de création d'un fichier de sauvegarde
        """
        d = datetime.fromtimestamp(path.stat().st_mtime)
        wd = (d.strftime("%A")+"___________________")[:10]
        return wd + d.strftime("%d/%m/%Y à %H:%M")
        
    p = pathlib.Path(MEDIA_ROOT, "sauvegarde")
    sauvegardes_faites = {
        MEDIA_URL + "sauvegarde/" + path.name: sauvegarde_date(path) \
        for path in p.glob('*.zip')
    }
    return render_common(
        request,'gestion/sauvegarde.html',
        _("Sauvegarder la base de données SLM"),
        {
            "aide": "assistance#sauvegarde",
            "sauvegardes": sorted(
                sauvegardes_faites.items(),
                key = lambda x: x[0],
                reverse = True),
        })
    
@ensure_csrf_cookie
@i18n
def messages_modif(request):
    """
    Permet de voir et d'éditer les messages prédéfinis
    """
    numero = request.POST.get("numero","")
    clonage = request.POST.get("clonage","")
    saved = False
    # l'instance à modifier sera définie à deux conditions : que le
    # paramètre numero ne soit pas vide, et que ce numéro identifie bien
    # un enregistrement existant.
    instance = Courriers.objects.filter(
        N = int(numero)) if numero else None
    instance = instance[0] if instance else None
    # si le paramètre modif_db n'est pas vide, alors le formulaire
    # est repris depuis les valeurs renseignées dans la page web
    # cependant que la clé primaire viendra toujours de l'instance
    # (que celle-ci soit définie ou non) ; si l'instance est indéfinie
    # ça provoquera la création d'un nouvel enregistrement
    form = Courriers_Form(
        request.POST if request.POST.get("modif_db", "") else None,
        instance=instance)
    # test de validité avant d'enregistrer
    if form.is_valid():
        form.save()
        saved = True
    # affichage de l'interface web
    return render_common(
        request,'gestion/messages_modif.html',
        _("Les messages prédéfinis"),
        {
            "numero": numero,
            "clonage": clonage,
            "form": form,
            "saved": saved,
        })
    
@i18n
def donnees_utilisateur(request):
    """
    Modification des données utilisateur
    """
    if request.POST:
        form= donnees_utilisateur_Form(request.POST)
        if form.is_valid():
            request.user.last_name = form.cleaned_data.get("nom")
            request.user.first_name = form.cleaned_data.get("prenom")
            request.user.email = form.cleaned_data.get("email")
            if form.cleaned_data["passe1"]:
                request.user.set_password(form.cleaned_data["passe1"])
            request.user.save()
            return redirect(form.cleaned_data["next"])
    else:
        next = request.GET.get("next")
        form= donnees_utilisateur_Form({
            "nom": request.user.last_name,
            "prenom": request.user.first_name,
            "email": request.user.email,
            "next": next,
        })
    return render_common(
        request,'gestion/donnees_utilisateur.html',
        _("Mise à jour des données personnelles pour {user}").
        format(user = request.user.username),
        {
            "form": form,
        })

@i18n
def importeFromGemaSCO(request):
    """
    Importation de fichiers issus de GemaSCO
    """
    modeles = {}
    for mod in modeles_ordre:
        # création d'un arbre de dépendances
        the_class = eval(mod)
        related = [f.related_model.__name__ for f in the_class._meta.fields \
                   if isinstance(f, django.db.models.fields.related.ForeignKey)]
        modeles[mod] = {
            "the_class": the_class,
            "related": json.dumps(related),
            "csvfile": os.path.basename(les_modeles[mod]["csvfile"]),
        }
    return render_common(
        request,'gestion/importeFromGemaSCO.html',
        _("Importation de données de GemaSCO"),
        {
            "modeles": modeles,
            "names": list(modeles.keys()),
        })

@ensure_csrf_cookie
@i18n
def inscription_provisoire(request):
    """
    Affiche la page pour l'inscription provisoire des élèves ;
    Paramètres de GET ou POST: "code_temporaire"
    Paramètres de POST: tous ceux qui sont gérés par le formulaire
    Eleves_Form
    """
    code_temporaire = request.GET.get("code_temporaire","")
    eleve = None
    if code_temporaire:
        eleves = Eleves.objects.filter(
            autorisation_provisoire_inscription=code_temporaire)
        if eleves:
            eleve = eleves[0]
        else:
            eleve = Eleves(
                Nom_de_famille = "À changer",
                Prenom = "À changer",
                classe = Classes.objects.get(libelle=NOCLASS),
                autorisation_provisoire_inscription=code_temporaire,
            )
            eleve.save()
        form = Eleves_Form(instance=eleve)
        # le code reste valide pour recommencer seulement 30 minutes
        code_valide = datetime.fromisoformat(code_temporaire) > \
            datetime.now() - timedelta(minutes=30)
        return render(request, "gestion/inscription_provisoire.html",{
            "page_pour_tous": True,
            "form": form,
            "code_temporaire": code_temporaire,
            "code_valide": code_valide,
            "saisie": True,
        })
    else:
        code_temporaire = request.POST.get("code_temporaire","")
        fin_validite = datetime.fromisoformat(code_temporaire) + \
            timedelta(minutes=30)
        eleve = Eleves.objects.get(autorisation_provisoire_inscription=code_temporaire)
        form = Eleves_Form(request.POST, instance=eleve)
        ok = form.is_valid()
        if ok:
            eleve = form.save(commit=False)
            eleve.a_verifier = True
            eleve.NomPrenomEleve = eleve.Nom_de_famille + " " + eleve.Prenom
            eleve.save()
        return render(request, "gestion/inscription_provisoire.html",{
            "page_pour_tous": True,
            "saisie": False,
            "code_temporaire": code_temporaire,
            "fin_validite": fin_validite,
            "eleve": eleve,
        })
    
@i18n
def invite_inscription_provisoire(request):
    """
    Crée une autorisation temporaire pour saisir les coordonnées d'un nouvel
    élève, et communique avec l'élève par un lien direct ou par QR-code
    """
    d = datetime.now()
    fin_validite = d + timedelta(minutes=30)
    dernier_autorise = d - timedelta(minutes=30)
    # on annule les champs autorisation_provisoire_inscription des élèves
    # qui ont dépassé la fin de validité, à moins qu'ils aient déjà
    # des livres prêtés
    # on efface d'abord les élèves qu'on n'a pas vérifiés dans les 30 minutes
    eleves_perimes = Eleves.objects.filter(
        a_verifier=True,
        autorisation_provisoire_inscription__lt=dernier_autorise)
    for e in eleves_perimes:
        if not(Prets.objects.filter(eleve = e)):
            # il n'y a pas de livres prêtés pour l'élève e, on l'efface
            # ainsi que sa caution automatique
            c = Cautions.objects.get(numero = e.no_caution)
            c.delete()
            e.delete()
        else:
            # au moins un livre a été prêté, l'élève est considéré comme
            # vérifié
            e.a_verifier=False
            e.autorisation_provisoire_inscription = None
            e.save()
    eleves_ok = Eleves.objects.filter(
        a_verifier=False,
        autorisation_provisoire_inscription__lt=dernier_autorise)
    for e in eleves_ok:
        e.autorisation_provisoire_inscription = None
        e.save()
    # on fait la liste des élèves à vérifier : ils auront un lien de mise à jour
    eleves_a_verifier = Eleves.objects.filter(a_verifier=True)
    code_temporaire = d.isoformat()
    url = request.scheme + "://" + \
        request.META["HTTP_HOST"] + \
        "/inscription_provisoire?code_temporaire=" + str(code_temporaire)
    buffer = BytesIO()
    qr = pyqrcode.create(url)
    qr.svg(buffer, scale=8)
    svg = buffer.getvalue().decode().replace(
        '<?xml version="1.0" encoding="UTF-8"?>', '')
    return render_common(
        request, "gestion/invite_inscription_provisoire.html",
        _("Invitation à l'auto-inscription"),
        {
            "aide": "auto_inscription",
            "url": url,
            "fin_validite": fin_validite,
            "svg": svg,
            "eleves_a_verifier": eleves_a_verifier,
        })

@ensure_csrf_cookie
@i18n
def print_etat_classe(request):
    """
    produit un état des prêts par élèves pour une classe,
    prêt à imprimer depuis le navigateur
    POST contient le paramètre classe
    """
    classe = request.POST.get("classe")
    date = datetime.now().strftime("%Y-%m-%d")
    eleves = Eleves.objects.filter(Lib_Structure = classe).order_by('NomPrenomEleve')
    return render(
        request,
        "gestion/print_etat_classe.html",
        {
            "classe": classe,
            "date": date,
            "eleves": OrderedDict( ((e, ("doit" not in e.doit)) for e in eleves) )
        },
    )
    
@ensure_csrf_cookie
@i18n
def livres_pour(request):
    """
    Présente une page imprimable, pour les livres correspondant à une
    catégorie (2D, 1stmg, 1G, ...) ; POST fournit ce paramètre
    """
    categorie = request.POST.get("categorie")
    livres = Materiel.objects.filter(categories__contains = categorie)
    prix_total = sum((float(liv.Prix.replace(",", ".")) if liv.Prix \
                      else 0.0 for liv in livres))
    return render(
        request,
        "gestion/livres_pour.html",
        {
            "livres": livres,
            "categorie": categorie,
            "prix_total": f'{prix_total:.2f}'.replace(".", ","),
        },
    )
   
    
@ensure_csrf_cookie
@i18n
def cautions(request):
    """
    Sert la page qui permet de gérer les cautions versées par les élèves
    Le POST peut apporter les paramètres nomprenom, qui désigne un élève
    et operation qui peut être "", ou "annule' ou "rembourse"
    """
    nomprenom = request.POST.get("nomprenom", "")
    operation = request.POST.get("operation", "")
    eleve = None
    ok = False
    msg = ""
    if nomprenom:
        eleves = Eleves.objects.filter(NomPrenomEleve = nomprenom)
        if eleves:
            eleve = eleves[0]
            ok = True
        else:
            msg = f"L'élève « {nomprenom} » n'a pas été trouvé dans la base"
    return render_common(
        request, 'gestion/cautions.html', _("Cautions"),
        {
            "aide": "cautions",
            "eleve": eleve,
            "operation": operation,
            "msg": msg,
            "ok": ok,
        }
    )
    
@i18n
def revue_du_stock(request):
    """
    Méthode d'inventaire adaptée à la revue du stock
    """
    return render_common(
        request, 'gestion/revue_du_stock.html', _("Revue du stock"),
        {
            "aide": "revue_du_stock",
        }
    )
    
@i18n
def eleve_scan(request):
     """
    Page qui permet aux élèves de faire leur "shopping" dans la réserve
     """
     return render_common(
        request, 'gestion/eleve_scan.html', _("Recherche de livres"),
         {
             "aide": "eleve_scan",
         }
     )

@i18n
def eleve_invite(request):
    """
    Page qui permet aux élèves de visiter la réserve comme une boutique.
    
    Affiche un QR_code, et l'URL correspondante : tous deux dirigent vers
    une page d'accueil, où un jeton périssable passera dans les cookies,
    pour vérifications utltérieures.

    Elle permet aussi d'afficher le QR-code sur une page imprimable.
    """
    # on crée un jeton de durée une heure et on l'enregistre.
    j = Jeton.create(3600)
    j.save()
    request.session["jeton"] = str(j.id)
    # et on fait un peu de ménage à l'occasion
    Jeton.housekeep()
    baseUrl = f"{request.scheme}://{request.get_host()}/"
    url = baseUrl + f"boutique?jeton={j.id}"
    return render_common(
        request, 'gestion/eleve_invite.html', _("Invitation"),
        {
            "url": url,
            "qrcode": urlToQrcode(url),
            "jeton": j,
            "aide": "eleve_invite",
        }
    )
    
@i18n
def boutique(request):
    """
    Entrée dans la Coop en mode boutique
    On inscrit le jeton dans les cookies, et on propose à l'élève
    d'entrer son nom, puis on lui fait choisir son lot de livres
    à demander.
    """
    jetons = Jeton.objects.filter(id = request.GET.get("jeton"))
    if jetons:
        jeton = jetons[0]
        request.session["jeton"] = str(jeton.id)
    else:
        jeton = None
    return render_common(
        request, 'gestion/boutique.html', _("Boutique de livres"),
        {
            "jeton": jeton,
            "aide": "boutique",
        },
        pour_tous = True,
    )

@i18n
def jeton_perime(request):
    """
    Page qui apparaît quand on visite une page protégée par le décorateur
    @assure_jeton_valide et que le jeton de session est périmé.
    """
    return render_common(
        request, 'gestion/jeton_perime.html', _("Il est trop tard"),
        {
            "jeton": request.session.get("jeton"),
            "aide": "jeton_perime",
        },
        pour_tous = True,
    )
    
@assure_jeton_valide
@ensure_csrf_cookie
@i18n
def moisson(request):
    """
    Moisson de livres, par un élève. Le POST contient une liste de livres
    """
    abreges = request.POST.get("livres")
    abreges = abreges.split(",")
    textes = request.POST.get("txt")
    if textes:
        textes = [t.strip() for t in textes.split(",")]
    else:
        textes = len(abreges) * [""]
    manuels = []
    for a,t in zip(abreges, textes):
        ids = [m.id for m in Materiel.objects.filter(abrege = a)]
        manuels.append({"abrege": a, "ids": ids, "txt": t})
    tous_manuels = []
    tous_abreges = {m.abrege for m in Materiel.objects.all()}
    for a in tous_abreges:
        ids = [m.id for m in Materiel.objects.filter(abrege = a)]
        tous_manuels.append({"abrege": a, "ids": ids})
    return render_common(
        request, 'gestion/moisson.html', _("Chercher les livres"),
        {
            "jeton": Jeton.objects.get(id = request.session.get("jeton")),
            "manuels": manuels,
            "tous_manuels": tous_manuels,
            "aide": "moisson",
        },
        pour_tous = True,
    )

@i18n
def correlation_code(request):
    """
    Affiche du code Python pour entreprendre une analyse de corrélation entre
    les "options", c'est à dire les valeurs importées des champs SIÈCLE
    Lib_Mat_enseignee_x (x = 1 à 5), et les abrégés des livres réellement
    empruntés par une cohorte d'élèves.

    Le but est de construire um moteur qui étant donné une "option",
    prédit si un livre doit être emprunté, ou non. Ce code source Python
    est repris dans le fichier views_stats.py
    """
    options = set([e.Lib_Mat_Enseignee_1 for e in Eleves.objects.all()]) | \
         set([e.Lib_Mat_Enseignee_2 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_3 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_4 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_5 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_6 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_7 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_8 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_9 for e in Eleves.objects.all()]) | \
        set([e.Lib_Mat_Enseignee_10 for e in Eleves.objects.all()])
    options = sorted([ o for o in options if o])
    options = {v:i for i,v in enumerate (options)}
    nb_options = len(options)
    abreges = sorted(list(set(m.abrege for m in Materiel.objects.all() if m.abrege)))
    abreges = {v:i for i,v in enumerate(abreges)}
    nb_abreges = len(abreges)
    donnees_eleves = []
    n = 0
    for e in Eleves.objects.all():
        ## on s'intéresse aux élèves qui ont 3 prêts au moins seulement
        codes_livres_dus = e.dus_codes()
        if len(codes_livres_dus) < 4:
            continue
        n += 1
        ## on ne considère que les 10000 premiers pour le moment
        if n > 10000:
            break
        opt = [0]*nb_options
        for i in (2,3,4,5):
            # on considère les matières enseignées n° 2 à 5
            option = getattr(e, f"Lib_Mat_Enseignee_{i}")
            if option:
                opt[options[option]] = 1
        abg = [0]*nb_abreges
        for code in codes_livres_dus:
            i = abreges[Inventaire.objects.get(id = code).materiel.abrege]
            abg[i] = 1
        donnees_eleves.append((opt,abg))
    # beautification de donnees_eleves
    donnees_formatees = ""
    d = pformat(donnees_eleves, compact = True)
    for line in d.split("\n"):
        donnees_formatees += re.sub(r"(([01], ){26})", r"\1\n   ", line)+"\n"
    return render_common(
        request, 'gestion/correlation_code.html',
        _("Code Python de corrélation"),
        {
            "options": options,
            "abreges": abreges,
            "donnees": donnees_formatees,
            "date": datetime.now(),
            "aide": "correlation_code",
        },
    )  

@i18n
def correlation(request):
    """
    Met en évidence des corrélations entre les options et les manuels
    empruntés par les élèves
    """
    return render_common(
        request, 'gestion/correlation.html',
        _("Corrélation entre options et livres"),
        {
           "aide": "correlation",
        },
    )  
    
@i18n
def qr_boutique(request):
    """
    permet d'imprimer les étiquettes pour les QR-codes de boutique
    """
    # estimation du nombre d'étiquettes, en limitant à 70 cm de hauteur
    # soit deux piles de 35 cm, chaque empilement qui correspond à une
    # référence dans le catalogue
    abreges = {m.abrege: m.id for m in Materiel.objects.all()}
    # attention, plusieurs m.id peuvent avoir un même abrégé, seul le
    # dernier m.id sera conservé ici
    nombre_double_piles = {}
    for a in abreges:
        h=0
        for m in Materiel.objects.filter(abrege = a):
            h += m.epaisseur_mm * len(Inventaire.objects.filter(materiel = m))
        nombre_double_piles[a] = math.ceil(h/700)
    piles = [
        {"abrege": a,
         "code": abreges[a],
         "nombre": nombre_double_piles[a]}
        for a in sorted([ab for ab in abreges],
                        key=lambda x: sans_accent(x).lower())
    ]
    premiere_pile =  None
    livre_exemple =  None
    nombre = 0
    if piles:
        premiere_pile = piles[0]
        lex = Materiel.objects.filter(abrege=premiere_pile["abrege"])
        if lex:
            livre_exemple = lex[0]
            nombre = len(Inventaire.objects.filter(
                materiel__abrege = livre_exemple.abrege))
    return render_common(
        request, 'gestion/qr_boutique.html',
        _("QR-Codes pour le passage en boutique"),
        {
            "piles": piles,
            "premiere_pile": premiere_pile,
            "livre_exemple" : livre_exemple,
            "nombre": nombre,
            "aide": "qr_boutique",
        },
    )  
    
@i18n
def prof_livre_invite(request):
    """
    Page qui permet aux profs de proposer, pendant 96 heures, des
    prêts de livres bien spécifiques à certains élèves
    
    Affiche un QR_code, et l'URL correspondante : tous deux dirigent vers
    une page d'accueil, où un jeton périssable passera dans les cookies,
    pour vérifications utltérieures.

    Elle permet aussi d'afficher le QR-code sur une page imprimable.
    """
    # on crée un jeton de durée une heure et on l'enregistre.
    j = Jeton.create(96*3600)
    j.save()
    request.session["jeton"] = str(j.id)
    # et on fait un peu de ménage à l'occasion
    Jeton.housekeep()
    baseUrl = f"{request.scheme}://{request.get_host()}/"
    url = baseUrl + f"livre_supplement?jeton={j.id}"
    livres_supplement = LivreSupplement.objects.all()
    return render_common(
        request, 'gestion/prof_livre_invite.html', _("Livres à emprunter"),
        {
            "url": url,
            "qrcode": urlToQrcode(url),
            "jeton": j,
            "livres_supplement": livres_supplement,
            "aide": "prof_livre_invite",
        }
    )
    
@assure_jeton_valide
@i18n
def livre_supplement(request):
    return render_common(
        request,
        'gestion/livre_supplement.html',
        "Livre en supplément",
        {
            "aide": "livre_supplement",
        },
        pour_tous = True,
    )

@i18n
def livre_supplement_go(request):
    data = request.POST.get("data")
    commentaire = request.POST.get("commentaire")
    ls = LivreSupplement(data = data, commentaire = commentaire)
    ls.save()
    data = json.loads(data)
    livres = [Materiel.objects.get(id = int(id)) for id in data["livres"]]
    eleves = [Eleves.objects.get(id = int(id)) for id in data["eleves"]]
    baseUrl = f"{request.scheme}://{request.get_host()}/"
    return render_common(
        request,
        'gestion/livre_supplement_go.html',
        _("Livre en supplément"),
        {
            "aide": "livre_supplement_go",
            "data": data,
            "livres": livres,
            "eleves": eleves,
            "baseUrl": baseUrl,
        },
        pour_tous = True,
    )
    
@i18n
def verif_plan(request, filename):
    """
    Vérifie qu'un plan contient des données correctement organisées. Le plan
    est un fichier au format SVG. Dans ce plan, on est censé trouver des
    groupes qui contiennent un rectangle et un texte commençant par "classes:" ;
    ces groupes sont nommés « étagères » dans la mesure où on y entrepose les
    livres destinées à un groupe de classes. D'autre part on y trouve aussi des
    groupes contenant un rectangle, un texte et un cercle ; ces groupes ont
    un attribut "class" qui a la valeur "livres", on les appellera « piles » ;
    ils dénotent le placement d'une ou de plusieurs piles de livres,
    le texte est l'abrégé correspondant aux livres à y trouver, le
    cercle à un « repère lumineux » qui devra être proche de
    l'étiquette matérielle à scanner, dénotant le même abrégé.
    =======
    On vérifiera :
    1) que tous les abrégés du fichier SVG correspondent à des abrégés
       enregistrés dans des instances de models.Materiel ;
    2) que les piles sont bien contenues dans une et une seule
       étagère ;
    3) que le placement d'une pile dans une étagère peut être
       considérée comme cohérente avec un couple niveau/abrégé d'une
       instance de models.Materiel au moins.
    =======
    @param filename un chemin vers un fichier SVG, relatif à
           settings.MEDIA_ROOT, mais sans le suffixe .svg
    """
    svgfiles=[]
    svg=""
    svgfile = MEDIA_ROOT + "/" + filename + ".svg"
    if filename and os.path.exists(svgfile):
        svgfiles=[]
        svg = mark_safe(open(svgfile, encoding="utf-8").read())
    else:
        cp = run(f"find {MEDIA_ROOT} -name '*.svg'", shell=True,
                 encoding="utf-8", capture_output = True)
        svgfiles = [f.replace(MEDIA_ROOT+"/", "") for \
                    f in cp.stdout.strip().split("\n")]

    liste_abreges = sorted(
        list(set([
            m.abrege for m in Materiel.objects.all() if m.abrege.strip()])),
        key = lambda x: unidecode.unidecode(x).lower()
    )
    return render_common(
        request,
        'gestion/verif_plan.html',
        _("Vérification de plan"),
        {
            "aide": "verif_plan",
            "filename": filename + ".svg",
            "liste_abreges": json.dumps(liste_abreges),
            "svg": svg,
            "svgfiles": svgfiles,
        })

@i18n
def une_etiquette(request, numero):
    """
    Permet d'imprimer une étiquette pour un livre, après choix de la
    position pour l'imprimante
    @param numero le numéro d'inventaire du livre
    """
    livre = Inventaire.objects.get(id = int(numero))
    manuel = livre.materiel
    return render_common(
        request,
        'gestion/une_etiquette.html',
        _("Imprimer une étiquette"),
        {
            "aide": "une_etiquette",
            "numero" : numero,
            "manuel": manuel,
            "etiquettes_cols": range(3),
            "etiquettes_ligs": range(8),
        })
    
@i18n
def eleves_plus_n(request, niveau, nombre):
    """
    Donne une liste des élèves d'un niveau qui doivent plus de n livres
    @param niveau seconde, premiere ou terminale
    @param nombre nombre de livres dus au moins (zéro pour au moins un livre)
    """
    debut = "2"
    n=""
    if niveau == "secondes":
        debut = "2"
        n = "seconde"
    elif niveau == "premieres":
        debut = "1"
        n = "première"
    elif niveau == "terminales":
        debut = "T"
        n = "terminale"
    """
    eleves = Eleves.objects.filter(classe__libelle__startswith = debut).\
        order_by("classe__libelle", "sans_accent")
    plus_n = {
        e: len(Prets.faits_a(e)) for e in eleves if \
           len(Prets.faits_a(e)) > int(nombre)
    }
    """
    plus_n = {
        e: len(Prets.faits_a(e)) for e in \
        Eleves.avec_prets_qs(int(nombre)).\
        filter(classe__libelle__startswith = debut).\
        order_by("classe__libelle", "sans_accent")
    }
    return render_common(
        request,
        'gestion/eleves_plus_n.html',
        _("Élèves qui doivent des livres"),
        {
            "aide": "eleves_plus_n",
            "eleves": plus_n,
            "n": n,
            "nombre": nombre,
            "date": date.today(),
        })
        
@i18n
def abreges(request, svgfile, multiplicateur):
    """
    Prépare l'impression d'étiquettes pour les abrégés à placarder
    au niveau des piles de livres
    @param svgfile le nom d'un fichier svg où se trouvent localisés tous les
           abrégés (dans des groupes de classe "livres"), positionnés sur
           des étagères.
    @param multiplicateur le nombre d'étiquettes à imprimer par défaut
           pour chaque groupe de classe "livres"
    """
    multiplicateur = float(multiplicateur)
    svg = etree.parse(os.path.join(MEDIA_ROOT, svgfile))
    etiquettes = svg.xpath('//*[name()="svg"]' +
                           '//*[name()="g"][@class="livres"]' +
                           '/*[name()="text"]')
    tous_abreges = [" ".join(e.xpath('*[name()="tspan"]/text()')) for e in etiquettes]

    abreges = OrderedDict()
    for a in sorted(tous_abreges, key = lambda x: unidecode.unidecode(x).lower()):
        if a in abreges:
            abreges[a] += 1
        else:
            abreges[a] = 1
    for a in abreges:
        abreges[a] *= multiplicateur
        abreges[a] = int(abreges[a])
    return render_common(
        request,
        'gestion/abreges.html',
        _("Imprimer les étiquettes d'abrégés"),
        {
            "aide": "abreges",
            "abreges": abreges,
            "svgfile": os.path.basename(svgfile),
            "multiplicateur": multiplicateur,
        })

def choix_eleves(request):
    """
    Crée des sélections d'élèves pour faire des traitements automatisés
    avec la liste choisie, comme imprimer des étiquettes de courrier, etc.
    """
    return render_common(
        request,
        'gestion/choix_eleves.html',
        _("Actions sur des élèves à choisir"),
        {
            "aide": "choix_eleves",
        })

def action_courrier(request):
    """
    Gestion d'étiquettes d'adresses postales pour des élèves choisis
    Le POST contient un paramètre noms
    """
    noms = request.POST.get("noms")
    #eleves = Eleves.objects.filter(NomPrenomEleve__in = noms)
    return render_common(
        request,
        'gestion/action_courrier.html',
        _("Étiquettes pour des courriers aux élèves"),
        {
            "aide": "action_courrier",
            "noms": noms,
            "lignes": list(range(1, 1+8)),
            "colonnes": list(range(1, 1+3)),
        })

    
def essai_svg(request):
    """
    page expérimentale pour bricoler des graphes de dépendances
    """
    return render_common(
        request,
        'gestion/essai_svg.html',
        _("essai SVG"),
        {
            "aide": "",
        })
