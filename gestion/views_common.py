"""
    gestion.views_common, un module pour SLM
    - partie commune du rendu pages web à l'aide de modèles

    Copyright (C) 2023 Georges Khaznadar <georgesk@debian.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from django.shortcuts import render
from django.utils.translation import gettext_lazy as _
from django.utils.translation import activate, get_language_info

from .version import this_version
from manuels.settings import language_for

import unicodedata

class Onglet:
    """
    Cette classe définit les données d'un onglet. Les paramètres du
    constructeur sont:
    @param title le titre de l'onglet
    @param page le nom de da fonction à invoquer dans le module views
    @param pour_tous vrai si l'onglet est défini pour tous (faux par défaut)
    """
    def __init__(self, title, page, pour_tous = False):
        self.title = title
        self.page = page
        self.pour_tous = pour_tous
        return
    
Les_onglets = [ ## accès aux pages de niveau 1
    Onglet(_("Paramètres"), "parametres"),
    Onglet(_("Achats/Inventaire"), "achats_inventaire"),
    Onglet(_("Prêts"), "prets"),
    Onglet(_("Retours"), "retours"),
    Onglet(_("Boutique"), "eleve_invite"),
    Onglet(_("Assistance"), "assistance"),
    Onglet(_("À propos"), "apropos", pour_tous = True),
]


def render_common(request, template, subtitle="", context=None,
                  content_type=None, status=None, using=None,
                  pour_tous=False):
    """
    rendu des pages incluant des onglets, basé sur django.shortcuts.render
    @param request l'objet requête
    @param template le modèle de page à rendre
    @param subtitle un sous-titre spécifique à la page à rendre
    @param context le contexte pour utiliser le modèle ; il sera enrichi
      pour produire le titre et les onglets
    @param content_type idem que pour django.shortcuts.render
    @param status idem que pour django.shortcuts.render
    @param using idem que pour django.shortcuts.render
    @param pour_tous permet de forcer le caractère public de la page ;
       si la page n'est pas publique, un message demande de s'authentifier
       (faux par défaut)
    @return un objet de type HTTPRequest
    """
    lalangue = request.session.get("lang", "en") # en par défaut
    activate(lalangue)
    dico = {
        "version": this_version,
        "nom_page": subtitle,
        "onglets": Les_onglets,
        "page_pour_tous": pour_tous,
        "lalangue": lalangue,
        "locale": language_for(lalangue),
    }
    if context is None:
        context = dico
    else:
        context.update(dico)
    if "aide" not in dico:
        dico["aide"] = True
    return render(request, template, context, content_type, status, using)
    
