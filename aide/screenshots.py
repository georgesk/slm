"""
    screenshots, un programme pour SLM
    - Crée des copies d'écran "reproductibles" pour le système d'aide
      lancement : ../manage.py shell -c "from aide.screenshots import screenshots; screenshots('content/images')"

Copyright (c) 2023-2024 Georges Khaznadar <georgesk@debian.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
"""

from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.contrib.auth.models import User
from django.utils.translation import gettext_lazy as _
from django.utils.translation import activate, get_language_info

from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select

from PIL import Image
from io import BytesIO

from gestion.models import Eleves, Inventaire, Materiel
from manuels.settings import language_for, LANGUAGES
from aide.annotation import annote, TextArrow

import os

CREDENTIALS = {
    'username': 'testuser',
    'password': 'secret'
}

class LiveServer(StaticLiveServerTestCase):

    fixtures = ['fixtures/test_data.json']

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.service = ChromeService(executable_path="/usr/bin/chromedriver")
        options = Options()
        cls.ui_visible = True
        options.add_argument("--window-size=800,2000")
        if not os.environ.get("VIEW_SELENIUM") == "1":
            options.add_argument('--headless')
            print("WARNING: to display Chrome's UI, set VIEW_SELENIUM=1")
            cls.ui_visible = False
        cls.selenium = webdriver.Chrome(
            options = options, service = cls.service)
        cls.selenium.implicitly_wait(10)
        return

    def setUp(self):
        """
        Ajoute un utilisateur 'testuser'
        """
        StaticLiveServerTestCase.setUp(self)
        u = User.objects.create_user(**CREDENTIALS)
        u.is_staff = True # important, car le login est à /admin/login
        u.save()
        return
        
    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        return

# drapeaux pour le paramètre verbosity    
FILES = 1
ACTIONS = 2

class Action:
    """
    Représentations d'actions qui se déroulent sur le serveur selenium
    Paramètres du constructeur :
    @param args plusieurs chaînes à évaluer (après l'ajout de self. au début)
    @param lang une langue pour l'image générée (None par défaut)
    @param kw un dictionnaire de mots-clés ; les mots-clés importants
    peuvent être :
    - fname un nom de fichier pour y enregistrer une capture d'écran
    - lang (None par défaut); "en" donnera "en-us", "es" donnera "es-es", etc.
      si le mot-clé suivant, lang_in_path est activé
    - lang_in_path s'il vaut vrai, on utilise des répertoires comme en-us/, etc.
    - post_shot liste de traitements à faire après la copie d'écran et avant
      d'enregistrer une image; liste vide par défaut
    - annotations (facultatif, liste vide par défaut) est une liste d'instances
      de annotation.TextArrow qui sera utilisée pour annoter la capture
      d'écran. Cette liste peut avoir été générée par l'utilitaire
      svg2annotations
    """
    def __init__(self, *args, **kw):
        self.actions = args
        self.fname = None
        self.lang = None
        self.lang_in_path = False
        self.post_shot = []
        self.annotations = ""
        for k in kw:
            setattr(self, k, kw[k])
        return
    
        
class ScreenshotScenario:

    def __init__(self, *args, **kw):
        """
        définit un scenario pour une série de captures d'écran
        @param args une série d'instances d'Action
        @param kw un dictionnaire de mots-clés, tels que "verbosity"
               par exemple
        """
        self.scenario = args
        self.verbosity = 0
        for k in kw:
            setattr(self, k, kw[k])
        self.ls = LiveServer()
        self.ls.setUpClass()
        self.shots = []
        return

    def __del__(self):
        self.ls.tearDownClass()
        return

    def pause(self, duration = 0.5):
        #un peu d'attente
        ActionChains(self.ls.selenium).pause(duration).perform()
        return
    
    def get(self, url):
        """
        primitive de scenario :
        chargement d'une page web
        @param url une URL absolue, du serveur temporaire
        """
        url = self.ls.live_server_url + url
        if ACTIONS & self.verbosity:
            print("getting the url :", url)
        self.ls.selenium.get(url)
        self.pause()
        return

    def click(self, css_selector=None):
        """
        primitive de scenario :
        clic sur un élément de page web facultatif
        @param css_selector un sélecteur qui renvoie un élément unique
        """
        if css_selector:
            self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector).click()
        else:
            ActionChains(self.ls.selenium).click().perform()
        self.pause()
        return

    def send_keys(self, css_selector, string):
        """
        primitive de scenario :
        envoie des frappes de clavier vers un élément de page web
        @param css_selector un sélecteur qui renvoie un élément unique
        @param string une chaîne de caractères à taper
        """
        self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)\
                     .send_keys(string)
        return

    def move_to_element_with_offset(self, css_selector, dx, dy):
        """
        primitive de scenario :
        déplace le curseur de souris à un élément donné, plus un décalage
        @param css_selector un sélecteur qui renvoie un élément unique
        @param dx décalage en abscisse
        @param dy décalage en ordonnée
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        ActionChains(self.ls.selenium)\
            .move_to_element_with_offset(elt, dx, dy).\
            perform()
        return
    
    def move_to_element_with_offset(self, css_selector, dx, dy):
        """
        primitive de scenario :
        déplace le curseur de souris à un élément donné, plus un décalage
        @param css_selector un sélecteur qui renvoie un élément unique
        @param dx décalage en abscisse
        @param dy décalage en ordonnée
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        ActionChains(self.ls.selenium)\
            .move_to_element_with_offset(elt, dx, dy).\
            perform()
        return

    def dragRectangle(self,css_selector, x1, y1, x2, y2):
        """
        primitive de scenario :
        déplace le curseur de souris à un élément donné, plus un décalage,
        fait un clic gauche maintenu, et va à un autre décalage.
        @param css_selector un sélecteur qui renvoie un élément unique
        @param x1 décalage en abscisse du départ
        @param y1 décalage en ordonnée du départ
        @param x2 décalage en abscisse de l'arrivée
        @param y2 décalage en ordonnée de l'arrivée
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        ActionChains(self.ls.selenium)\
        .move_to_element_with_offset(elt, x1, y1)\
        .click_and_hold()\
        .move_to_element_with_offset(elt, x2, y2)\
        .perform()
        return

    def drag_and_drop_by_offset(self,css_selector, x2, y2):
        """
        primitive de scenario :
        déplace le curseur de souris à un élément donné,
        fait un clic droit maintenu, déplace le curseur à un autre
        décalage, et relâche le clic.
        @param css_selector un sélecteur qui renvoie un élément unique
        @param x2 décalage en abscisse du départ
        @param y2 décalage en ordonnée du départ
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        ActionChains(self.ls.selenium)\
        .drag_and_drop_by_offset(elt, x2, y2)\
        .perform()
        return

    def drag_and_drop(self, css_selector1, css_selector2):
        """
        primitive de scenario :
        fait un tirer-glisser du centre d'un élémént à un autre
        @param css_selector1 un sélecteur pour le premier élément
        @param css_selector2 un sélecteur pour le deuxième élément
        """
        elt1 = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector1)
        elt2 = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector2)
        ActionChains(self.ls.selenium)\
        .drag_and_drop(elt1, elt2)\
        .perform()
        return

    def scroll_by_amount(self, dx, dy):
        """
        primitive de scenario :
        scrolle depuis la position courante, avec un décalage
        @param dx décalage
        @param dy décalage
        """
        ActionChains(self.ls.selenium)\
        .scroll_by_amount(dx, dy)\
        .perform()
        return
    
    def scroll_to_element(self, css_selector):
        """
        primitive de scenario :
        déplace le curseur de souris à un élément donné,
        fait un clic droit maintenu, déplace le curseur à un autre
        décalage, et relâche le clic.
        @param css_selector un sélecteur qui renvoie un élément unique
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        ActionChains(self.ls.selenium)\
        .scroll_to_element(elt)\
        .perform()
        return

    def select_by_visible_text(self, css_selector, text):
        """
        primitive de scenario :
        sélectionne une option d'un élément select
        @param css_selector un sélecteur qui renvoie un élément SELECT
        @param text le texte de l'option à sélectionner
        """
        elt = self.ls.selenium.find_element(By.CSS_SELECTOR, css_selector)
        Select(elt).select_by_visible_text(text)
        return
     
    def reset_actions(self):
        """
        remet à zéro toutes les actions jouées
        """
        ActionChains(self.ls.selenium)\
            .reset_actions()
        return
    
    def joue(self, path):
        """
        joue le scenario sel.scenario avec le serveur selenium self.ls
        @param path chemin vers les fichiers de copies d'écran
               il peut être modifié si lang_in_path == True
        """
        for actionInstance in self.scenario:
            for a in actionInstance.actions:
                if ACTIONS & self.verbosity:
                    print("déroulement de", a)
                eval("self." + a)
            if actionInstance.fname:
                # on s'assure qu'il y a le bon répertoire
                path_ = path
                if self.lang_in_path and actionInstance.lang:
                    path_ = os.path.join(
                        path, language_for(actionInstance.lang))
                if not os.path.exists(path_):
                    os.makedirs(path_, exist_ok=True)
                # on initialise une image avec une capture d'écran
                img = Image.open(
                    BytesIO(self.ls.selenium.get_screenshot_as_png()),
                    formats = ["png"])
                for ps in actionInstance.post_shot:
                    img = self.retraite_image(img, ps)
                if actionInstance.annotations:
                    if actionInstance.lang:
                        # modification de la langue pour localisation
                        print("GRRRR activating", actionInstance.lang)
                        activate(actionInstance.lang)
                    img = annote(img, *actionInstance.annotations)
                # on enregistre l'image
                target = os.path.join(path_, actionInstance.fname)
                img.save(target)
                self.shots.append(target)
                if FILES & self.verbosity:
                    print("copie d'écran vers :", target)
                if not actionInstance.lang:
                    # copies vers chacun des sous-répertoires de langue
                    for lang, _ in LANGUAGES:
                        target = os.path.join(path_, lang, actionInstance.fname)
                        img.save(target)
                        self.shots.append(target)
                        if FILES & self.verbosity:
                            print("copie d'écran vers :", target)
        self.reset_actions()
        return

    def retraite_image(self, img, action):
        """
        Modifie une image
        @param img une instance de PIL.Image
        @param action une chaîne à évaluer, qu'on concatènera à "img."
        """
        result = eval("img." + action)
        return result

def screenshots(path = "/tmp"):
    """
    Usage:
    ./manage.py shell -c "from gestion.screenshots import *; screenshots(['<path>'])"
    @param path chemin vers où placer les copies d'écran
    """
    langs = ("fr", "es", "en")
    # ouvre la page des paramètres, sans authentification
    page_param = Action("get('/parametres')")
    # montre le dialogue d'identification en plusieurs langues
    authentification1 = [
        Action(f"click('#language-{lang} img')",
               fname = "authentification1.png",
               lang = lang,
               post_shot = ["crop((228, 314, 228+344, 314+91))"])
        for lang in langs
    ]
    # suite de l'authentification en plusieurs langues
    authentification2 = [
        Action("get('/parametres')", f"click('#language-{lang} img')",
               "click('#contents a')",
               fname = "authentification2.png",
               lang = lang,
               post_shot = ["crop((203, 99, 203+393, 99+290))"]
               )
        for lang in langs
    ]
    # ouvre la page d'accueil sans authentification
    page_defaut = Action("get('/apropos')")
    # trois copies d'écran dans trois langues
    accueil1 = [
        Action(f"click('#language-{lang} img')",
               fname = "accueil1.png",
               lang = lang)
        for lang in langs
    ]
    # authentification, dans cette même page d'accueil
    login = Action(
        f"send_keys('#id_username', '{CREDENTIALS['username']}')",
        f"send_keys('#id_password', '{CREDENTIALS['password']}')",
        "click('form .submit-row input')"
    )
    # trois copies d'écran, dans cette page après authentification
    accueil2 = [
        Action(f"click('#language-{lang} img')", fname = "accueil2.png",
               lang = lang)
        for lang in langs
    ]
    parametres =  Action("get('/parametres')")
    parametres1 = [
       Action(
           f"click('#language-{lang} img')", fname = "parametres1.png",
            post_shot = ["crop((0, 0, 689, 1136))"],
            lang = lang)
        for lang in langs
    ]
    eleves = Action("get('/eleves')")
    eleves1 = [
        Action(
            f"click('#language-{lang} img')", fname = "eleves1.png",
            post_shot = ["crop((0, 0, 556, 263))"],
            lang = lang)
        for lang in langs
    ]
    eleves2 = [Action(
        f"click('#language-{lang} img')",
        "send_keys('#nomprenom', 'ato')",
        "pause()",
        fname = "eleves2.png",
        post_shot = ["crop((0, 79, 297, 79+246))"],
        lang = lang)
        for lang in langs
    ]
    eleves3 = [Action(
        f"click('#language-{lang} img')",
        "send_keys('#nomprenom', 'ato')",
        "pause()",
        "click('.ui-menu-item div')",
        "pause()",
        fname = "eleves3.png",
        post_shot = ["crop((0, 123, 815, 123+441))"],
        lang = lang)
        for lang in langs
    ]
    eleves4 = [Action(
        f"click('#language-{lang} img')",
        "send_keys('#classe', 'TG03')",
        "send_keys('#classe', Keys.ENTER)",
        "pause()",
        fname = "eleves4.png",
        post_shot = ["crop((0, 120, 399, 120+356))"],
        lang = lang)
        for lang in langs
    ]
    printFicheSuivi = Action(
        "send_keys('#nomprenom', 'ato')",
        "pause()",
        "click('.ui-menu-item div')",
        "pause()",
        fname = "print_fiche_suivi.png",
        post_shot = ["crop((547,258,547+118, 258+108))"],
    )
    cartemembre = Action("get('/cartes')")
    cartemembre1 = [
        Action(
            f"click('#language-{lang} img')",
            fname = "cartemembre1.png",
            lang=lang,
            post_shot = ["crop((0, 119, 663, 119+258))"],
        )
        for lang in langs
    ]
    cartemembre2 = [
        Action(
            f"click('#language-{lang} img')",
            "dragRectangle('.ui-selectee',-22, -2, 22, 62)",
            fname = "cartemembre2.png",
            lang=lang,
            post_shot = ["crop((0, 209, 184, 209+353))"],
        )
        for lang in langs
    ]
    cartemembre3 = [
        Action(
            f"click('#language-{lang} img')",
            "drag_and_drop_by_offset('.ui-selectee', 22, 62)",
            "scroll_by_amount(0, 300)",
            "pause()",
            fname = "cartemembre3.png",
            lang=lang,
            post_shot = ["crop((0, 30, 356, 30+331))"],
        )
        for lang in langs
    ]
    cartemembre4 = [
        Action(
            "get('/cartes')",
            f"click('#language-{lang} img')",
            "drag_and_drop('.ui-selectee', '#page_footer')",
            "pause()",
            "click('#imprimables button')",
            "pause()",
            "select_by_visible_text('#max_nb', '3')",
            "pause()",
            "click('#preview')",
            fname = "cartemembre4.png",
            lang=lang,
            post_shot = ["crop((0, 176, 538, 176+792))"],
            annotations = [
                TextArrow(302.8, 717.3, 222.2, 256.8, 676.1,
                          _('''ici une liste de classes visées''')),
                TextArrow(121.8, 497.7, 198.2, 208.8, 531.8,
                          _('''nombre de pages à prévoir''')),
                TextArrow(134.3, 386.5, 143.3, 358.0, 416.5,
                          _('''combien de cartes sur la première page''')),
                TextArrow(410.7, 369.6, 131.5, 474.8, 407.2,
                          _('''visualiser avec ce nombre de cartes''')),
                TextArrow(275.9, 255.2, 178.2, 306.3, 117.9,
                          _('''détail du nombre de cartes imprimées sur chaque page'''))
            ]
        )
        for lang in langs
    ]
    constantes = Action("get('/constantes')")
    constantes1 = Action(
        fname="constantes1.png",
        post_shot = ["crop((35, 257, 35+112, 257+95))"],
    )
    constantes2 = Action(
        fname="constantes2.png",
        post_shot = ["crop((23, 457, 23+69, 457+62))"],
    )
    constantes3 = Action(
        fname="constantes3.png",
        post_shot = ["crop((175, 941, 175+68, 941+62))"],
    )
    #### rendu des actions #######################
    scenar = ScreenshotScenario(
        #page_param,
        #*authentification1, *authentification2,
        page_defaut,
        #*accueil1,
        login,
        #*accueil2,
        #parametres,
        #*parametres1,
        #eleves,
        #*eleves1,
        #*eleves2,
        #*eleves3,
        #*eleves4,
        #printFicheSuivi,
        #cartemembre,
        #*cartemembre1,
        #*cartemembre2,
        #*cartemembre3,
        #*cartemembre4,
        constantes,
        constantes1,
        constantes2,
        constantes3,
        verbosity = FILES, #FILES|ACTIONS,
        lang_in_path = True,
    )
    scenar.joue(path)
    #print("Copies d'écran créées :", scenar.shots)
    return
