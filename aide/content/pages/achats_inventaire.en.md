---
Title: Stock/Inventory
slug: achats_inventaire
layout: page
date: 2023-04-12
lang: en-us
translated: true
---

The second one is linked to the page "Stock/Inventory"

![paramètres]({static}../images/achats_inventaire.jpg){.shadowed}

Some features are present there:

## « Inventory » <a name="inventaire"></a>

![paramètres]({static}../images/inventaire0.jpg){.shadowed}

This feature is for:

- finding whom was lent a book, or whether this book is meant to remain in the stock;
- displaying lists of books, either lent or not, eventually selected by a title;
- displaying lists of students, which were lent books or not, eventually select by their class name;

### Research for a book<a name="recherche_livre"></a>

One types (or flashes) the code of the book in the upper part of the page, and its status is displayed.

![paramètres]({static}../images/inventaire1.jpg){.shadowed}

### Books, not lent<a name="livres_nonpretes"></a>

This feature is part of "accordions" which can be unfolded or folded back.
When one clicks on the gray bar, it becomes blue and a list of books
(not lent) appears; the first 40 first, then more if one scrolls down.

One can precise the title of a book: type three characters, and a choice of
matching titles appears. When one clicks in the menu below the input
field, the title is selected, and the list is evaluated again.

**N. B.** : if on clicks a book number which appear in the list, its status
is displayed in the top part of the window, under the title 
*Whom is this book?*

![paramètres]({static}../images/inventaire2.jpg){.shadowed}

### Books (lent to somebody)<a name="livres_pretes"></a>

This second "accordion" works much alike the previous one, but it displays
book which are currently lent to somebody, as announced in the bar's
title.

### Students owing at least one book<a name="eleves_pretes"></a>

The third accordion is to list students; the input field above the display
area if for selecting eventually a class, and one just need to type two 
characters, and select one class in the menu which appears below.

**N. B.** : if one clicks on a student name in the display area, the window
about loans is opened, for the selected student.

![paramètres]({static}../images/inventaire3.jpg){.shadowed}

### Students owing no books<a name="eleves_nonpretes"></a>

The fourth accordion works like the third one, with the difference that
if displays students owing no books.

## "Catalog" <a name="catalogue"></a>

This feature leads to the catalog of books managed by SLM.

![paramètres]({static}../images/catalogue0.jpg){.shadowed}

For every book title existing in the database, one can:

- ![paramètres]({static}../images/catalogue1.jpg){.shadowed} delete the title
- ![paramètres]({static}../images/catalogue2.jpg){.shadowed} modify the file about this title
- ![paramètres]({static}../images/catalogue3.jpg){.shadowed} clone the file about this title, for example to declare a new publication
-  ![paramètres]({static}../images/catalogue4.jpg){.shadowed} add books with this title in the books' inventory, and create bar-code labels accordingly
- print the list of titles managed by SLM
- create and save the file for a new title

For the two last features in the list above, one uses the bigger buttons
in the upper left:

![paramètres]({static}../images/catalogue5.jpg){.shadowed}

## Add books in the inventory and manage their bar-codes <a name="ajout_codes"></a>

![paramètres]({static}../images/codes_barres.jpg){.shadowed}

This page displays previously generated bar-code labels and to print them
again when necessary, or else one can also create new bar-code labels.
As soon as new bar-code labels are generated, the same number of book records
is appended to the inventory, and one can immediately create printable label
sheets.

## Print bar-code labels <a name="impression_codes"></a>

This feature is available from the web page
["Catalogue"](#catalogue) ...

When one clicks on an active button, during the previous step
(see above), a page appears to preview the sheets of printable bar-code labels.

![paramètres]({static}../images/impression_codes_barres.jpg){.shadowed}

Before printing the sheets, one must undergo a "preview" step, which
allows one to customize the layout of the first sheet, where labels will
be printed (supported sheets are with a geometry 3 columns x 8 rows,
with reference "Avery J8159"). *Warning:* only the first sheet can be
defined with non-printable areas; for every sheet beyond, the layout of
labels goes on without blank places. Of course, on the last sheet, final
places can remain blank.

**N. B.** : at the end, when one clicks on the print button, a PDF file
is created, and one can view it in the browser. It can also be downloaded and
managed with another PDF-enabled software.

## "Tariff" <a name="tarifs"></a>

This feature is not yet implemented, for SLM version 1.2.0

It is meant to update tariff systems which apply when some book is lost
or damaged.

## "Purchase orders" <a name="bon_commande"></a>

This feature is not yet implemented, for SLM version 1.2.0

It is meant to manage purchase orders to buy books.

