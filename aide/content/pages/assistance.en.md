---
Title: Assistance
slug: assistance
layout: page
date: 2023-04-12
lang: en-us
translated: true
---

This page gathers a few sub-features which could not be classified elsewhere.


![Assistance]({static}../images/assistance0.jpg){.shadowed}

## "Help" <a name="aide"></a>

When one clicks on the "Help" link, the present main page of the user
manual opens.

**N. B.**: the star with five spikes, always visible at the right of
the window, an becomes less fuzzy when hovered by the mouse cursor,
is the entry point to a *contextual* help; when one clicks this star,
in the context of the page of the screenshot above... so, one can read
this very line.

## "Save" <a name="sauvegarde"></a>

A backup of the database is recommenced each time when a consequent
work is achieved, for example at the end of a day's work.

![Backup]({static}../images/sauvegarde.jpg){.shadowed}

Currently, only the button "SQLite format" provides a reliable backup.
One downloads a file named, for instance, `db.sqlite-2023-04-12_17-03-45.zip`,
which is in ZIP format, and provides after decompression a file named
`db.sqlite3` .his file contains all of the database; it allows an administrator
with write permissions on the system supporting the service SLM, to restore
its state exactly as it was at the backup's date.

So, with the file named `db.sqlite-2023-04-12_17-03-45.zip`, one can
restore the state as it was on the 12th April 2023 at 17:03:45.

## "Maintenance" <a name="maintenance"></a>

*Ordinary* users of SLM, who are part of the team which can authentify,
cannot make much with this feature; the maintenance of the database
is only possible for users who have "super-user" permissions.

![no maintenance]({static}../images/admin0.jpg){.shadowed}

If you are an *ordinary* user of SLM, but that you know some mean to
become "super-user", you logout from SLM, and then re-authentify with
the accreditation (login name and password) of the super-user.

Here is the appearance of the screen when one has those permissions;
then, one can maintain directly the database: arbitrarily delete data,
add new ones, and so on.

![maintenance]({static}../images/admin1.jpg){.shadowed}

**Making a backup is highly recommended** for the database before
beginning any maintenance. Actions made by super-users are logged in the
database, as long as they are using the maintenance system provided by
Django, the framework which powers SLM.

