---
Title: Apoyo
slug: assistance
layout: page
date: 2023-04-12
lang: es-es
translated: true
---

Esta función agrega algunas subfunciones que no podían ir en otra parte.


![Apoyo]({static}../images/es-es/assistance0.jpg){.shadowed}

## "Ayuda" <a name="aide"></a>

Cuando se hace clic en el enlace "Ayuda", aparece la presente página del
manual del usuario.

**N. B.**: la estrello con cinco puntas, siempre visible en la derecha arriba
de la página, y que es más visible cando el cursor está sobre ella, dirige
hasta una ayuda *contextual*; si se hace clic en esta estrella, en el contexto
de la página de la captura de pantalla por encima... Pues bien, se lee esta
línea precisamente.

## Copia de seguridad <a name="sauvegarde"></a>

Está bien de realizar copias de seguridad de la base de datos cada vez
que un trabajo consecuente fue hacho, por ejemplo al fin de un día de
trabajo.

![Copia de seguridad]({static}../images/es-es/sauvegarde.jpg){.shadowed}

Corrientemente, únicamente el botón "formato Sqlite" permite una copia de
seguridad fiable. Luego se descarga un archivo llamado por ejemplo
`db.sqlite-2023-04-12_17-03-45.zip`, al formato ZIP y que da después de
descomprimirlo, un archivo llamado `db.sqlite3`. Este archivo
contiene la base de datos completa; permite a un administrador que tiene
derechos en el servidor que mantiene el servicio SLM, de restaurar su
funcionamiento exactamente como era al momento de la copia de seguridad.

Con el ejemplo del archivo llamado`db.sqlite-2023-04-12_17-03-45.zip`,
se puede restaurar el estado de la base de datos, tal cual era el
12 abril 2023 a las 17 horas 03 minutos y 45 segundos.

## "Mantenimiento" <a name="maintenance"></a>

Un usuario "ordinario" de SLM, miembro del equipo, que puede autenticar
como tal, no puede hacer mucho cuando hace clic en "Mantenimiento";
en efecto, el mantenimiento de la base de datos es reservado a los usuarios
que tienen derechos de "súper-usuario".

![no mantenimiento]({static}../images/admin0.jpg){.shadowed}

Si eres un usuario *ordinario* de SLM, pero conoces una acreditación
de "súper-usuario", puedes desconectar de SLM, y luego volver a autenticarte
con esta acreditación (nombre de usuario y contraseña) de súper-usuario.

Aquí está una captura de pantalla cuando se tiene el derecho de súper-usuario;
se puede mantener directamente la base de datos: borrar datos arbitrariamente,
añadir nuevos datos, etc.

![mantenimiento]({static}../images/es-es/admin1.jpg){.shadowed}

**Se recomienda fuertemente** de hacer una **copia de seguridad** de la base de
datos antes de empezar cualquier mantenimiento. Las acciones de cada
súper-usuario están jornalados en la base de datos, si se utiliza únicamente
la herramienta de mantenimiento que viene con el software Django (él que
ánima el servicio web SLM).

