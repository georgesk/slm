---
Title: Summary of help
slug: landing-page-about-hidden
layout: page
date: 2023-04-11
status: hidden
lang: en-us
translated: true
---

- [SLM as a software](pages/slm/index.html)
- [Home page](pages/page-accueil/index.html)
- [Authentication](pages/pages-autres-authentification/index.html), for other pages
- [Parameters](pages/parametres/index.html)
- [Stock/Inventory](pages/achats_inventaire/index.html)
- [Loans](pages/prets/index.html)
- [Returns](pages/retours/index.html)
- [Assistance](pages/assistance/index.html)
