---
Title: Parámetros
slug: parametres
layout: page
date: 2023-04-11
lang: es-es
translated: true
---

La pestaña de izquierda es para la página de "Parámetros"

![parámetros]({static}../images/es-es/parametres1.png){.shadowed}

Algunas funciones son accesibles allá:

##  "Base de estudiantes" <a name="base_eleves"></a>

Cuando los datos externos, por ejemplo de la base SIÈCLE son importados,
SLM dispone de una lista de alumnos al día. La función "Base de estudiantes"
permite de encontrar el registro de un alumno si se conoce parte de su
apellido o de su nombre, o si se conoce su clase.

Si un alumno viene y es desconocido de la base de datos, puede ser porque
está nuevo en la escuela, y que su registro en la base del establecimiento
(SIÈCLE por supuesto) no fue importado ya. En tal caso (que deber ser
excepcional), se puede invitar el alumno a crear por sí mismo una entrada
en la "Base de estudiantes". Para hacerlo, hay que pasar por
<a href="../auto_inscription/index.html">"Parámetros → Matriculación provisional..."</a>

![Base de estudiantes]({static}../images/es-es/eleves1.png){.shadowed}

La captura de pantalla arriba es la página "Base de estudiantes" cuando
nada fue entrado ya. Si se teclea tres letras, en el campo del nombre
de alumno, una lista de posibilidades aparece...

![menú]({static}../images/es-es/eleves2.png){.shadowed}

.. y después se elige un nombre, y que se lo valida, aparece el registro del
alumno, que se puede modificar a veces.

![datos del alumno]({static}../images/es-es/eleves3.png){.shadowed}

Si se entra el nombre de una clase en el campo de entrada vecino, aparece
una lista de alumnos;

![clase de TG03]({static}../images/es-es/eleves4.png){.shadowed}

haciendo clic sobre una línea con un alumno de la lista,
aparece su recuerdo también.

<a name="suivi_individuel"></a>
![Datos del alumno]({static}../images/es-es/eleves2.png){.shadowed}

Arriba está el registro de un alumno. Véase el botón de acción
![botón de hoja de seguimiento]({static}../images/es-es/print_fiche_suivi.png){.shadowed}
para imprimir la hoja de seguimiento del alumno corriente.

**Atención** : si quieres hacer una *modificación*, esta no es copiada
inmediatamente en la hoja de seguimiento; se debe validar y registrar los
datos modificados, y después volver a abrir el registro del alumno para
finalmente imprimir los datos del alumno en sur hoja de seguimiento.

## "Tarjetas de membresía" <a name="cartes_membre"></a>

Cuando eliges esta función, aparece la página de loas tarjetas de membresía,
en su estado inicial, como en la captura de pantalla por debajo:


![Estado inicial de la página de tarjetas]({static}../images/es-es/cartemembre1.png){.shadowed}

El botón de impresora, en el estado inicial, permite imprimir el **revés**
de las tarjetas de membresía, solamente. *Se puede regresar al estado
inicial, volviendo por el menú de parámetros, si es necesario.*

Un poco mas abajo, se puede seleccionar un conjunto de clases.

  - haciendo clic en una clase selecciona esta y de-selecciona las otras;
  - haciendo Control-clic en una clase toca a esta clase sin de-seleccionar
    las otras;
  - haciendo clic y arrastrando, se puede seleccionar en una operación
    un largo conjunto de clases.

Cuando una clase, o mas, están seleccionada(s), el botón de la impresora
sirve para imprimir las tarjetas de membresía (no el revés: las tarjetas
tendrán los datos personales de cada alumno). Véase por abajo dos capturas
de pantalla, una *durando* el arrastro, la otra *inmediatamente después*:
las clases "premières G" fueron seleccionadas.


![arrastrando]({static}../images/es-es/cartemembre2.png){.shadowed}

![selección hecha]({static}../images/es-es/cartemembre3.png){.shadowed}

### "Tarjetas de membresía", cuando un conjunto de clases está seleccionado <a name="cartes_membre_selection"></a>

Cuando unas clases están seleccionadas, el botón de impresora sirve para
ir a un otro dialogo, donde se precisa si algunas tarjetas ya están
arrancadas de la hoja con perforaciones: efectivamente, si permanecen
entre dos y cinco tarjetas imprimibles en una hoja, esto debe ser gestionado
por salvar hojas.

![organización de la primera página]({static}../images/es-es/cartemembre4.png){.shadowed}

La captura de pantalla por arriba mostra algunas zonas:

  - la lista de clases cuyas tarjetas de membresía son tratadas;
  - la cuenta de hojas que serán imprimidas;
  - la posibilidad de arreglar la disposición de la primera página;
    por ejemplo aquí, la cuenta de tarjetas imprimibles es 3 -- esta
    cuenta puede variar entre 2 y 6 tarjetas. Hacer clic en el botón
    "Vista previa" para refrescar el detalle de la disposición de las
    tarjetas.

El botón de impresora sirve para descargar las tarjetas de membresía,
dispuestas como fue arreglado.

## "Registro provisional" <a name="inscription"></a>

La procedura de registro provisional debe ser utilizada de modo excepcional
únicamente, cuando un alumno viene, sin que sus datos ya fuesen importados
desde la base del establecimiento (SIÈCLE por supuesto).

<a href="../auto_inscription/index.html">Haga clic aquí</a> para más detalles

## "Depósitos" <a name="cautions"></a>

Aquí se pueden gestionar los registros de depósitos de alumnos:

  - imprimir más páginas del libro de depósitos
  - gestionar el depósito de un alumno existente el la base de datos

Cada vez que un alumno nuevo es creado o importado desde una base de
datos extraña (SIÈCLE por supuesto), un número de depósito es creado
para sus datos. Este número de depósito aparece en las hojas de seguimiento
anuales y en las tarjetas de membresía. Las líneas del libro de depósitos
jamás son borradas, incluso cuando un alumno desaparece de la base de datos
de SLM, por ejemplo cuando se va del establecimiento escolar y ha devuelto
todos sus libros.

Cada depósito tiene una fecho de creación, y una fecho de devolución
(que será valida después de la devolución efectiva). Se puede definir
depósitos con valor cera, de tal modo que el alumno sea "sin depósito";
en este caso la fecha de devolución no importa.

<a href="../caution/index.html">Haga clic aquí</a> para más detalles

## "Constantes" <a name="constantes"></a>

Hay tres secciones en la página: "Constantes del establecimiento",
"Mensajes predefinidos" y "Imprimir las hojas de seguimiento"

### Constantes del establecimiento <a name="contantes_etab"></a>

Se trata de definir el nombre del establecimiento, su dirección, los
nombres de sus jefes, etc.

Botones por grupo de tres como aquí:
![botones]({static}../images/es-es/constantes1.png){.shadowed}
tienen respectivamente las funciones:

  1. Borrar el registro de la línea corriente;
  2. Editar el registro de la línea corriente;
  3. Crear una copia del registro de la línea corriente y editarla.

Al fin de las líneas de constantes, el botón más largo
![botón plus]({static}../images/es-es/constantes2.png){.shadowed}
sirve para crear un registro nuevo y vacío y editar su contenido.

### Mensajes predefinidos <a name="messages"></a>

Allá son definidos los mensajes que aparecen en algunos documentos
imprimibles que SLM propone: por ejemplo hojas de préstamo de libros.

Hay botones de acción parecidos a los previos, con funciones parecidas.
 
### Imprimir hojas de seguimiento <a name="fiches_suivi"></a>

Finalmente, la tercera parte de la página sirve para imprimir la hojas de
seguimiento de alumnos. Lo botones de acción
![botón de hoja de seguimiento]({static}../images/es-es/constantes3.png){.shadowed}
sirven para generar un archivo PDF de las hojas de seguimiento; el primero,
más largo, sirve para generar todas la hojas de seguimiento (típicamente
algunos cientos); otros botones, más pequeños, sirven para generar la hojas
de una clase (típicamente algunas decenas).

Si se quiere imprimir pocas hojas de seguimiento, para pocos alumnos,
el mejor pasar por la página de la
<a href="#suivi_individuel">"Base de estudiantes"</a>.

## "Purgar préstamos" <a name="purger"></a>

Leyes (como la francesa *Informatique et libertés*) disponen que sean
borrados los datos personales después de dicho tiempo; esto vale para datos de
préstamos, y es preciso también, porque cuando un libro fue prestado y devuelto
no se necesita guardar la historia de cualquier persona que le haya tenido.

Esta función sirve para seleccionar los préstamos-devueltas, i.e.
operaciones las fechas de préstamo y devolución están validas. Cuando
una secuencia de fechas está definida, aparece la cuenta de operaciones
que se podrían "olvidar". Cuando se "olvida" una operación, es definitivo
en la base de datos corriente; sin embargo copias de la base de datos
pueden ser hechas previamente.

## "Importar SIÈCLE" <a name="siecle"></a>

SIÈCLE es el nombre de la base de datos corrientemente utilizada en
establecimientos escolares públicos en Francia. Esta función sirve para
sincronizar los alumnos conocidos en la base de datos de SLM.

Para importar un archivo SIÈCLE, se debe exportarlo en formato CSV, con
código UTF-8. Este trabajo será preferidamente hecho por un administrador
de la base de datos SLM, porque la función está desarrollada corrientemente,
en la versión 1.2 de SLM.
