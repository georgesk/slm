---
Title: Cautions
slug: caution
layout: page
date: 2023-09-13
lang: fr-fr
---

Quand on arrive sur la page des cautions, voici la copie de l'écran :

![cautions]({static}../images/caution1.png){.shadowed}

Le bouton avec une icône d'imprimante permet de télécharger la dernière
version du cahier de cautions au format imprimable PDF. Ce cahier n'évolue
que par ajout de nouvelles lignes et de nouvelles pages le cas échéant. On
peut donc utilement imprimer et conserver les pages complètes du cahier.

Quand on renseigne le champ « Élève » avec plus de trois caractères,
on peut choisir le nom d'un élève précis. Dès ce cela est fait, le bouton
d'impression est caché, et des actions deviennent possibles pour gérer
la ou les cautions de l'élève.

## Restaurer un ancien numéro<a name="restaurer"></a>

![ancienne caution]({static}../images/caution2.png){.shadowed}

Pour les élèves dont le numéro de caution est antérieur à l'utilisation du
logiciel SLM, le numéro de caution est positionné à zéro ; si on dispose du
numéro de caution ancien, par exemple en consultant un registre écrit
précédemment, il est alors possible d'entrer sa valeur dans le champ
« Ancien numéro de caution ». On peut préciser facultativement une date
au format AAAA-MM-JJ. Quand on valide, une cofirmation est demandée, puis
le nouveau numéro de caution est ajouté dans la base de données. Les anciens
numéros de caution (dont la valeur est inférieure à 15000) ne sont pas inscrits
dans le cahier de cautions que gère SLM ; cependant, ils apparaissent sur les
documents imprimables, tels que la fiche de suivi, les fiches de prêt,
de retour, et la fiche de mémoire en cas de perte de livres.

## Gestion ordinaire<a name="gestion"></a>

Quand l'élève choisi a un numéro de caution non-nul, l'écran a l'aspect
suivant :

![caution ordinaire]({static}../images/caution3.png){.shadowed}

Les rôles des boutons d'action sont respectivement :

  1. Neutraliser la caution, pour que l'élève soit « sans caution »
  2. « Rendre la caution » : c'est ce qu'on fait quand la caution est restituée
     à l'élève, totalement ou en partie. Dans le dialogue qui apparaît, on
     précise la somme rendue, qui peut être différente du montant de la caution,
     si par exemple un livre a été perdu
  3. Éditer un commentaire : un dialogue apparaît, qui permet d'éditer un
     bref commentaire relatif à la gestion de la caution de cet élève.
     *Le commentaire n'apparaît pas dans le livre de cautions imprimé*.

## Cautions successives<a name="sequence"></a>

![cautions successives]({static}../images/caution4.png){.shadowed}

Un élève peut avoir plus d'une mention dans le cahier de cautions, avec
des numéros différents. La copie d'écran ci-dessus montre le cas d'un élève
dont la caution n° 12400 a été rendue, parce qu'il avait quitté l'établissement
scolaire, pensant qu'il n'y reviendrait pas, et la caution n° 15604 a été créée
lors de sa réinscription.


