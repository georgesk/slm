---
Title: La page d'accueil
slug: page-accueil
layout: page
date: 2023-04-11
lang: fr
---

Supposons que SLM fonctionne comme un serveur séparé
qui sert ses pages web à la (fausse) adresse https://exemple.slm.org ; quand on
visite ce site directement, on arrive à la page d'accueil :

![page d'accueil de SLM]({static}../images/page_accueil.jpg){.shadowed}

Cette page est visible par tous : il n'est pas besoin de s'authentifier
pour la voir. Elle correspond à l'onglet « À propos de SLM », qui est 
entre les onglets « Paramètres »,  « Achats/Inventaire »,  « Prêts et retour »
à gauche, et « Assistance » à droite. Ces autres onglets mènent à des pages
qui nécessitent une authentification.

