---
Title: Paramètres
slug: parametres
layout: page
date: 2023-04-11
lang: fr-fr
---

L'onglet de gauche mène à la page des « Paramètres »

![paramètres]({static}../images/parametres1.png){.shadowed}

Plusieurs fonctions sont accessibles, à partir de là :

## « Base Élèves »<a name="base_eleves"></a>

Quand l'import SIÈCLE a été fait, SLM dispose d'une liste d'élèves à jour.
La fonction « Base Élèves » permet de retrouver la fiche d'un élève si
on connaît une partie de son nom ou de son prénom, ou si on connaît
la classe dont il fait partie.

Si un élève se présente et qu'il est inconnu de la base, ça peut être
parce qu'il est nouveau dans le lycée, et que son enregistrement dans
la base SIÈCLE n'a pas encore été importé dans SLM. Dans un tel cas
(qui doit rester exceptionnel), il est possible d'inviter l'élève à
créer lui-même une entrée dans la « Base Élèves ». Pour cela, on passera
par <a href="../auto_inscription/index.html">« Paramètres → Inscription provisoire »</a>

![Base élèves]({static}../images/eleves1.png){.shadowed}

La copie d'écran ci-dessus représente la page « Base Élèves » quand on n'y
a encore rien saisi.

Dès qu'on saisit trois lettres trois lettres dans le champ **Élève**,
une liste de possibilités apparaît ...

![menu déroulant]({static}../images/eleves2.png){.shadowed}

...et quand on en sélectionne une, puis qu'on valide, on voit la fiche de
l'élève, et on peut la modifier si nécessaire.

![fiche de l'élève]({static}../images/eleves3.png){.shadowed}

Si on saisit un nom de classe, on voit une liste d'élèves ;

![classe de TG03]({static}../images/eleves4.png){.shadowed}

si on clique sur la ligne d'un élève de la liste, alors on accède à sa fiche,
ce qui revient au cas précédent.

<a name="suivi_individuel"></a>
![fiche de l'élève]({static}../images/eleves3.png){.shadowed}

Ci-dessus, la fiche d'un élève. On remarque le bouton d'action
![bouton de fiche de suivi]({static}../images/print_fiche_suivi.png){.shadowed}
qui sert à imprimer la fiche de suivi de l'élève courant.

**Attention** : si on veut faire une *modification*, elle n'est pas répercutée
tout de suite dans la fiche de suivi ; il faut enregistrer les données
modifiées, puis reprendre la fiche de l'élève et enfin on peut imprimer sa fiche de suivi.

## « Cartes de membre »<a name="cartes_membre"></a>

Quand on choisit cette fonction, on arrive à la page de gestion des
cartes de membre, dont voici un aperçu, dans l'état initial :


![État initial de la page des cartes]({static}../images/cartemembre1.png){.shadowed}


Le bouton d'imprimante permet à ce moment-là d'imprimer des **versos** de
cartes de membre, seulement. *On revient à cet état initial en repassant
par le menu des paramètres, s'il le faut.*

Un peu plus bas, il est possible de sélectionner un ensemble de classes.

  - un clic sur une classe sélectionne celle-ci et désélectionne les autres ;
  - un contrôle-clic sur une classe agit sur une classe sans désélectionner
    les autres ;
  - on peut à l'aide d'un tirer-glisser, sélectionner d'un seul mouvement un
    grand groupe de classes.

Sitôt qu'on a une, ou plusieurs classes sélectionnées, le bouton d'imprimante
sert à imprimer des cartes de membres (au recto : celles-ci seront pré-remplies
avec les données de chaque élève). Voyez ci-dessous deux copies d'écran, une
*pendant* le tirer-glisser, la deuxième *juste après* : les quatre premières G
du début de liste ont été sélectionnées.


![sélection en cours]({static}../images/cartemembre2.png){.shadowed}

![sélection terminée]({static}../images/cartemembre3.png){.shadowed}

### « Cartes de Membre », quand une sélection de classes existe<a name="cartes_membre_selection"></a>

Quand des classes sont sélectionnées, le bouton d'imprimante permet de passer
à un autre dialogue, où on précise s'il y a des cartes déjà détachées de la
page pré-perforée : en effet, s'il reste entre deux et cinq cartes imprimables
dans une page, il faut gérer cela pour éviter le gaspillage.

![disposition de la première page]({static}../images/cartemembre4.png){.shadowed}

La copie d'écran ci-dessus met en évidence plusieurs zones :

  - un rappel des classes pour lesquelles on fait des cartes de membre ;
  - le nombre de pages qui seront imprimées ;
  - une possibilité pour négocier la disposition de la première page ;
    par exemple, ici, le nombre de cartes imprimables est 3 -- on peut
    varier ce nombre entre 2 et 6 cartes. Un clic sur le bouton
    « Prévisualiser » remet à jour le détail de la disposition des
    cartes.

Le bouton d'imprimante permet de télécharger les pages de cartes de membre,
disposées selon les réglages en cours de validité.

## « Inscription provisoire »<a name="inscription"></a>

La procédure d'inscription provisoire ne doit être utilisée
qu'exceptionnellement, quand un élève se présente mais que ses données
n'ont pas encore été importées depuis la base SIÈCLE.

<a href="../auto_inscription/index.html">Cliquez ici</a> pour une fiche d'aide plus détaillée.

## « Cautions »<a name="cautions"></a>

C'est là qu'on peut gérer les enregistrements de cautions des élèves :

  - l'impression de nouvelles pages du cahier des cautions
  - la gestion de la caution d'un élève existant dans la base de données

À chaque fois qu'un nouvel élève est créé ou importé depuis une base Siècle,
un nouveau numéro de caution lui est attribué. Ce numéro de caution apparaît
sur les fiches de suivi annuelles et sur les cartes de membres. Il n'est pas
prévu qu'un enregistrement du livre de cautions soit effacé, même quand un
élève disparaît de la base de données de SLM, par exemple quand il quitte
l'établissement scolaire après avoir rendu tous ses livres.

Chaque caution a une date de création, et une date de restitution
(valide dès qu'on a géré le remboursement de la caution). Il est possible
de définir des cautions à coût zéro, auquel cas on dit que l'élève est
« sans caution » ; dans ce cas la date de restitution n'a pas d'importance.

<a href="../caution/index.html">Cliquez ici</a> pour une fiche d'aide plus détaillée.

## « Constantes » <a name="constantes"></a>

La page comprend trois sections : « Constantes de l'établissement »,
« Messages prédéfinis » et « Imprimer les fiches de suivi »

### Constantes de l'établissement<a name="contantes_etab"></a>

Il s'agit de définir le nom de l'établissement, son adresse, les noms des
dirigeants, etc.

Les boutons par groupe de trois comme ceci :
![boutons]({static}../images/constantes1.png){.shadowed}
ont pour fonctions respectives :

  1. Supprimer l'enregistrement de la ligne courante ;
  2. Éditer l'enregistrement de la ligne courante ;
  3. Créer une copie de l'enregistrement de la ligne courante et l'éditer.

À la fin des lignes de constantes, le bouton plus grand
![bouton plus]({static}../images/constantes2.png){.shadowed}
permet de créer un nouvel enregistrement vide et de passer en mode édition
pour lui donner du contenu.

### Messages prédéfinis<a name="messages"></a>
C'est là aussi qu'on définit les messages qui apparaîtront dans certains
documents imprimables que SLM permet d'obtenir : par exemple les fiches 
de prêt de livres.

On retrouve les mêmes bouton d'action que ci-dessus, avec des rôles identiques.

### Imprimer les fiches de suivi<a name="fiches_suivi"></a>

Enfin, la troisième partie de la page permet d'imprimer les fiches de suivi
des élèves. Les boutons d'action
![bouton de fiche de suivi]({static}../images/constantes3.png){.shadowed}
permettent de générer le fichier PDF des fiches de suivi ; le premier,
plus grand, permet de générer toutes les fiches de suivi (typiquement plusieurs
centaines) ; les boutons suivants, plus petits, permettent de générer les
fiches pour une classe (typiquement quelques dizaines).

Si on ne souhaite imprimer que quelques fiches de suivi, pour un nombre
limité d'élèves, il vaut mieux passer par la page de la
<a href="#suivi_individuel">« Base Élèves »</a>.

## « Purger des prêts » <a name="purger"></a>

La loi *Informatique et libertés* impose qu'on efface les données concernant
les personnes après un certain temps ; ça s'applique aux données des prêts
de livres, et c'est d'autant plus utile que quand un livre a été prêté puis
rendu, au bout de quelques années il n'est pas utile de conserver l'historique
de chaque personne qui l'a eu à sa disposition.

Cette fonction permet de sélectionner les prêts-rendus, c'est à dire
les opérations pour lesquelles ont connaît la date du prêt et la date
du retour.  Une fois qu'une fourchette de dates a été définie, on voit
le nombre d'opérations qu'on peut « oublier ». L'oubli est définitif
dans la base de données courante ; cependant des sauvegardes
antérieures de cette base de données peuvent avoir été faites.

## « Importer SIÈCLE »<a name="siecle"></a>

Siècle est le nom de la base de données communément utilisée par
les établissements scolaires publics en France. Cette fonction permet la
mise à jour des élèves connus dans la base de données de SLM.

Pour importer un fichier Siècle, il faut en faire un export au format CSV,
encodé en UTF-8. Il vaut mieux réserver ce travail à un administrateur
de la base de données de SLM, cette fonction étant en cours de mise au point,
dans le version 1.2 de SLM.



