---
Title: Resumen de la ayuda
slug: landing-page-about-hidden
layout: page
date: 2023-04-11
status: hidden
lang: es-es
translated: true
---

- [El software SLM](pages/slm/index.html)
- [Página principal](pages/page-accueil/index.html)
- [Autenticación](pages/pages-autres-authentification/index.html), para otras páginas
- [Parametros](pages/parametres/index.html)
- [Compras/inventario](pages/achats_inventaire/index.html)
- [Préstamos](pages/prets/index.html)
- [Devoluciones](pages/retours/index.html)
- [Apoyo](pages/assistance/index.html)
