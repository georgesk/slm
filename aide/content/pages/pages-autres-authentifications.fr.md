---
Title: L'authentification
slug: pages-autres-authentification
layout: page
date: 2023-04-11
lang: fr-fr
---

Quand on arrive sur les pages web autres que la page d'accueil,
une demande d'authentification apparaît :

![demande d'authentification]({static}../images/authentification1.jpg){.shadowed}

On doit alors de cliquer sur le lien « veuillez vous authentifier »

![l'authentification]({static}../images/authentification2.jpg){.shadowed}

La copie d'écran ci-dessus montre comment se présente la page 
d'authentification ; on a intérêt à utiliser les capacités du navigateur
pour retenir les mots de passes, si on travaille sur un ordinateur
correctement sécurisé. Dès qu'on est correctement authentifié, les autres
pages montrent leur contenu.

**N.B.** : il existe deux sortes d'utilisateurs,

1. les utilisateurs de l'équipe : ils ont accès à toutes les fonctions,
   à l'exception de l'administration directe de la base de données ;
2. les super-utilisateurs : ceux-là accèdent à toutes les fonctions,
   y compris l'administration directe de la base de données ; cette 
   possibilité entraîne des responsabilités, parce qu'une manœuvre
   irréfléchie peut rapidement endommager la base de données.
