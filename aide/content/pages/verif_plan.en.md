---
Title: Shelves_map
slug: verif_plan
layout: page
date: 2024-06-19
lang: en-us
translated: true
---

This page is to verify the map(s) of the room where books are stored.
Those maps are in SVG format, and some particular rectangles must belong
to identifiable groups:

<dl>
<dt>The shelves:</dt>
<dd>
the group containing the rectangle must also contain a text
beginning by <tt>classes:</tt> ... for instance,
"classes: Secondes": which may mean that the shelve in this map
is for storing books for students of "seconde".
</dd>
<dt>The heaps of books:</dt>
<dd>Those groups must have an attribute
<tt>class="livres"</tt>; in order to define such an attribute, one can
work with the graphic program <b>Inkscape</b> and edit the group in
SVG source mode (keyboard shortcut = Shift+Ctrl+X); additionally, each of
those groups must contain an element <tt>circle</tt> or <tt>ellipse</tt>
placed in order to symbolize a label which will identify the heap of books
in the shelve. The heaps of books must be included in identifies shelves.
</dd>
</dl>

## Choosing the map <a name="choix"></a>

When the web page is opened without any parameter, one must select
a SVG file among those who are available:


![with no parameter]({static}../images/verif_plan1.png){.shadowed}

## Verifying the map<a name="verif"></a>

When the map has been selected, it appears in the lower part of the page:


![with a parameter]({static}../images/verif_plan2.jpg){.shadowed}

- the link "<a>Choose another map</a>" allows one to come back to the
  selection step.
- the button <button>Check the book heaps</button> triggers a program which
  considers all the groups with the attribute  class="livres", and populates
  the area at the top right of the window with clickable buttons, which can
  be used to "light on" the labels of book heaps. This same program
  makes an attempt to validate that every book heap comes with correct
  short names, and that every short name existing in the database is
  placed somewhere in the map.
- the button <button>Check the shelves</button> triggers another program
  which provides some information about every identified shelf, and
  displays a hoverable button, to find the place of the shelf and shows a
  list of short names of book heaps which are included in this shelf. This
  program makes also an attempt to validate that every book heap is well
  included in some shelf in the map.
  
### Check the book heaps <a name="verif_piles"></a>

In the screenshot below, one can see what happens after clicking the
button <button>Check the book heaps</button>: 

- a list of buttons appear in the top right, each one for one of the
  short names used for book heaps. The button "allemand" has been
  clicked and red "lights" appear in the map at places where those
  book heaps are stored.
- the verification check boxes are:
    - "All the abbreviations are OK"
      <input type="checkbox" checked onclick="return false"/>: the box
      is checked, which means that short names in the map are all short names
      defined in the database.
    - "All the abbreviations are stored on the shelves"
      <input type="checkbox" onclick="return false"/>: the box is not
      checked,because some short names defined in the database are not
      available in the map; the list of abbreviations defined in the catalog
      but not available in the map is written on the right, under the
      purple buttons.
	
![with a parameter]({static}../images/verif_plan3.jpg){.shadowed}

### Check the shelves <a name="verif_etageres"></a>

In the screenshot below, one sees what happens when one clicks on the
button <button>Check the shelves</button> :

- a list of categories of shelf appears: "secondes",
  "premières", "terminales" and "premières technologiques". In every line
  of this list, there are one or a few  beige buttons; for instance, for
  the "terminales non technologiques", the button is labeled "g1200"
  (it is the identifier of the group for this shelf, in the SVG source code),
  and as on hovers this button "g1200", the shelf in the bottom left appears
  with a purple halo, so it can be located. Just after the button
  "g1200", there is a field with a list of short names of book heaps which
  can be found on the shelf of "terminales non technologiques".
- the checkbox "Every book heap is on an identified shelve"
  <input type="checkbox" checked onclick="return false"/> is checked, which
  means that no books heap is outside of an identified shelf.
  
![with parameters]({static}../images/verif_plan4.jpg){.shadowed}

## Annex: the SVG file used in the example <a name="annexe"></a>

<a href="{static}../images/plan_salle_e01_2.svg" target="_new">
![the map in SVG format]({static}../images/plan_salle_e01_2.svg){.shadowed title="Click to open in a new tab and easily zoom in and out, or save the file"}
</a>
