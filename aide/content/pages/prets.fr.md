---
Title: Prêts
slug: prets
layout: page
date: 2023-12-01
lang: fr-fr
---

Cette page est celle qui permet de gérer les prêts de manuels aux élèves
en début d'année scolaire

## « Prêts » <a name="prets"></a>

La fonction de prêts permet trois choses :

1. examiner la liste des manuels déjà prêtés à un élève,
2. ajouter des manuels pour les lui prêter.
3. imprimer la fiche de prêts, que l'élève devra signer

![prêts]({static}../images/prets1.jpg){.shadowed}

**Le mode d'emploi est le suivant :**

1. Taper trois lettre ou plus du nom ou du prénom de l'élève :
   une liste déroulante apparaît, où l'on peut choisir un nom
   d'élève complet ; sitôt que le nom complet est choisi, la liste 
   des livres qui lui sont déjà prêtés apparaît, et le curseur se
   positionne sur le champ de saisie du numéro de manuel ; il suffit alors
   de taper le numéro de manuel, ou de flasher son code-barre à l'aide
   de la douchette (lecteur de codes-barres).
2. Chaque fois qu'un numéro de manuel est saisi, il est ajouté à la liste
   des nouveaux manuels à prêter, éventuellement après une demande de
   confirmation (par exemple, si on prête deux manuels avec le même titre
   au même élève).
3. Quand la liste des nouveaux manuels à prêter est correcte, on clique sur
   le bouton d'enregistrement afin que ces manuels se mettent dans la liste
   des « prêts enregistrés ». Il reste encore possible de revenir à 
   l'étape précédente pour ajouter des livres (mais plus pour en retirer !).
4. Enfin, on clique sur le bouton d'impression pour créer la fiche de prêt
   imprimable. Celle-ci est téléchargée au format PDF. Si le navigateur est
   bien réglé, il est alors possible tout de suite de la relire, puis de
   l'envoyer à l'imprimante.
   
À l'étape 2, un repentir reste possible (« Manuels à prêter **ou pas ?** »)
quant aux manuels à prêter, tant qu'on n'a pas
validé la liste de nouveaux manuels à prêter.

La liste de prêts est réalisée sur la base des manuels déjà prêtés ;
il faut donc valider la liste de nouveaux manuels à prêter avant de demander
à imprimer la fiche de prêts.

## « Avenants » <a name="avenants"></a>

On peut faire un avenant si on ajoute un ou des nouveaux prêts, alors
qu'une fiche de prêts ancienne est déjà signée.

Si on en vient à ajouter de nouveaux livres à un prêt
**à une date ultérieure**, le logiciel détecte cette circonstance et propose
de rédiger un avenant à une fiche de prêts déjà imprimée. L'avenant est à
imprimer au dos de la fiche de prêt déjà signée par l'élève.

![retours]({static}../images/prets2.jpg){.shadowed}

La copie d'écran ci-dessus montre les deux nouveaux boutons pour
impression de documents, et l'infobulle qui correspond au bouton du
haut : « Ajouter un avenant pour 1 livre(s) ». Le bouton du haut sert
à imprimer un avenant en haut de page, le bouton du bas est utile si
un premier avenant a déjà été imprimé en haut de page ; dans ce cas,
l'avenant nouveau serait imprimé en bas de page.

## « Même classe » <a name="meme-classe"></a>

![élèves de la même classe]({static}../images/prets3.jpg){.shadowed}

À la droite de la page web, il y a un rappel de la liste d'élèves de la classe.
Cette liste d'élèves apporte quelques facilités :

  - chaque nom d'élève dans la liste porte un lien qui permet de prêter (ou de
    rendre) les livres de cet élève
  - la liste des « classe précédentes » où l'élève a été inscrit apparaît.

Dans le contexte des prêts, il y a des boutons d'action
(comme dans le copie d'écran ci-dessus) pour imprimer une carte de membre,
et pour passer directement à un rendu de livres.

