DESTDIR =

DBDIR = $(shell if [ -d /srv/manuels.freeduc.science ]; then echo /srv/manuels.freeduc.science; else echo .; fi)
SAVEDIR = $(DBDIR)/media/sauvegarde

LOCALES = --locale fr --locale en --locale es
POFILES = $(shell find locale -name "*.po")
MOFILES = $(patsubst %.po, %.mo, $(POFILES))

all: aide/output/index.html $(MOFILES)

aide/output/index.html : $(shell ls aide/content/pages/*)
	make -C aide html
	@for l in es en; do \
	  python3 outils/non_traduits.py aide/content/pages $$l; \
	done

$(MOFILES): $(POFILES)
	@python3 manage.py compilemessages | grep -v "up to date" || true

clean:
	find . -name "*~" | xargs rm -f
	rm -f $(MOFILES)

savedb:
	@cd $(DBDIR); python3 savedb.py $(DBDIR)

stats_dev:
	@echo "Décompte des lignes codées par l'auteur"
	@echo "======================================="
	@echo
	@total=0; \
	n=$$(cat $$(find manuels -name "*.py"| grep -v migrat) | wc -l); \
	echo "- $${n} lignes en langage Python (application manuels)."; total=$$((total + n)); \
	n=$$(cat $$(find gestion -name "*.py"| grep -v migrat) | wc -l); \
	echo "- $${n} lignes en langage Python (module gestion)."; total=$$((total + n)); \
	n=$$(cat $$(find gestion -name "*.html") | wc -l); \
	echo "- $${n} lignes en langage HTML."; total=$$((total + n)); \
	n=$$(cat $$(find gestion/static -name "*.css"| grep -v jquery) | wc -l); \
	echo "- $${n} lignes en langage CSS."; total=$$((total + n)); \
	n=$$(cat $$(find gestion/static -name "*.js"| grep -v jquery) | wc -l); \
	echo "- $${n} lignes en langage Javascript."; total=$$((total + n)); \
	echo; echo $$total lignes en tout.

rsync_upload: aide/output/index.html
	rsync -av --delete --exclude="db*" --exclude="*archive*" --exclude="media" * freeduc.science:/srv/manuels.freeduc.science/
	ssh freeduc.science "cd /srv/manuels.freeduc.science; echo 'yes' | ./manage.py collectstatic; ./manage.py migrate"
	@echo "prochaines commandes : ssh freeduc.science, puis sudo service apache2 reload, et synchronisation de media/"

MAKEMESSAGE_PARAMS = --extension "html,txt,py,rml" --ignore aide/output
messages:
	./manage.py makemessages $(LOCALES) $(MAKEMESSAGE_PARAMS)
	./manage.py compilemessages | grep -v "up to date"
