Dépendances de l'application web SML
====================================

python3-django              pour Django
gnome-colors-common         pour certaines icônes
python3-reportlab           pour générer les PDF et la prévisualisation SVG
python3-lxml                pour la prévisualisation SVG
fonts-liberation            pour les polices qu'utilise reportlab
fonts-liberation2           pour les polices qu'utilise reportlab
python3-svglib              pour la bidouille de fichiers svg
sound-icons                 pour disposer du son de xylophone
pelican                     pour générer les pages statiques de l'aide
python3-levenshtein         pour associer les noms de champs dans l'import CSV
python3-termcolor           pour des messages en couleur durant l'import CSV
python3-pyqrcode            pour les QR-codes de l'invitation à s'inscrire
python3-odf		            pour les listes d'élèves à problèmes
libjs-jquery                pour jQuery
libjs-jquery-ui             pour jQuery-UI
libjs-jquery-ui-touch-punch pour jQuery-UI : extension "touch"
qrencode                    pour créer des qrcodes à la volée (boutique)
python3-pandas              pour la corrélation options/livres
python3-pylabels            pour les étiquettes et autres imprimés répartis dans une page
node-html5-qrcode           pour une appli javascript qui lit les QR-codes
python3-unidecode           pour désaccentuer les noms
python3-trml2pdf            interprétation du format RML de Reportlab
python3-pypinyin            chinois -> latin désaccentué
node-svgdotjs-svg.js        pour svg.js

Modules d'Apache2, pour le serveur de production
================================================

apache2                   le serveur web. Il faut y activer quelques modules
...                       => module rewrite, à activer
...                       => module ssl, à activer

libapache2-mod-wsgi-py3   => module wsgi, à activer

Logiciels tiers embarqués
=========================

elegant
-------

**elegant** : un theme pour Pelican, publié sous licence MIT ; 
auteur : Talha Mansoor (https://github.com/talha131) ; liste des auteurs à 
https://github.com/Pelican-Elegant/elegant/graphs/contributors

Quelques modifications y ont été apportées, telles qu'un lien symbolique 
du thème vers jQuery et des ajustages dans les fichiers custom.css, custom.js

Paquets utiles pour le développement
====================================

python3-strictyaml          pour récupérer les métadonnées des fichiers .md
python3-selenium	    pour automatiser les captures d'écran de l'aide
xvfb                        pour que les captures d'écran soient reproductibles
python3-pil		    pour traiter les images de gestion.screenshots
python3-svg.path	    pour traiter les images de gestion.screenshots

