import qrcode
import base64
import io

from qrcode.image.styledpil import StyledPilImage
from qrcode.image.styles.moduledrawers.pil import RoundedModuleDrawer


"""
Prépare l'impression de quelques QR-codes pour des essais
"""
"""
data a été exporté le 20/12/2023, par :
data = {m.abrege: f"coop-{m.id}" for m in Materiel.objects.all()}
"""
data = {
 'gestion': 'coop-1',
 'manag.': 'coop-103',
 'droit': 'coop-102',
 'éco': 'coop-100',
 'Histoire géo-emc': 'coop-6',
 'maths': 'coop-73',
 'besch français': 'coop-8',
 'philo': 'coop-106',
 'espagnol': 'coop-98',
 'hist. géo.': 'coop-15',
 'r.h.': 'coop-17',
 'gestion finance': 'coop-18',
 'grammaire anglais': 'coop-50',
 'anglais': 'coop-87',
 'ses': 'coop-21',
 'physique chimie': 'coop-61',
 'svt': 'coop-23',
 'histoire': 'coop-53',
 'arabe': 'coop-36',
 'chinois': 'coop-29',
 'grec': 'coop-86',
 'lsf': 'coop-65',
 'cambridge euro': 'coop-33',
 'allemand': 'coop-84',
 'llce anglais': 'coop-58',
 'H.G.S.P.': 'coop-59',
 'physique chimie spé': 'coop-40',
 'ses spé': 'coop-62',
 'svt spé': 'coop-63',
 'Ens. scient.': 'coop-105',
 'm&t français': 'coop-49',
 'anglais seasons': 'coop-51',
 'géo': 'coop-85',
 'philo lire': 'coop-56',
 'cambridge test': 'coop-57',
 'littérature anglais': 'coop-60',
 'maths Spé': 'coop-64',
 'maths expertes': 'coop-66',
 'maths compl.': 'coop-67',
 'poche': 'coop-107',
 'llcer anglais': 'coop-74',
 'latin': 'coop-79',
 'mercatique': 'coop-81',
 'emc': 'coop-82',
 'calculette': 'coop-88',
 'chargeur': 'coop-89',
 'hlp': 'coop-92'}


if __name__ == "__main__":
    qrdivs = ""
    for k in sorted(data.keys(), key=lambda x: x.lower()):
        v = data[k]
        qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_H)
        qr.add_data(v)
        img = qr.make_image(
            image_factory=StyledPilImage,
            module_drawer=RoundedModuleDrawer()
        )
        buffer = io.BytesIO()
        img.save(buffer, format="PNG")
        buffer.seek(0)
        qrdivs += "<div style='display: inline-block; margin: 5px; " + \
            "vertical-align: top;'>"
        qrdivs += "<img style='width: 100px;' src=\"data:image/png;base64," + \
            f"{base64.b64encode(buffer.read()).decode('utf-8')}\"/>"
        qrdivs += "<p style='text-align: center; width: 100px; " + \
            "height: 50px; margin:0;'>{k}</p>"
        qrdivs += "<p style='text-align: center; font-size: 200%; " + \
            "margin:0;'>⇊⇊⇊⇊</p></div>\n"
    print (qrdivs)
        
