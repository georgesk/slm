"""
Parcourt la table des élèves pour supprimer les cautions en trop
de la table des cautions. Une caution est en trop quand il y en a
déjà une qui n'a pas été rendue !
"""
from gestion.models import *

def cautions_non_rendues(eleve):
    """
    Renvoie un queryset de cautions d'un élève donnée quand celles-ci
    sont non rendues
    """
    return Caution.objects.filter(nom = eleve.Nom, prenom = eleve.Prenom,
                                  rendue_le = DATE111).order_by("numero")

def eleves_avec_trop_de_cautions():
    """
    Affiche les élèves qui ont plus d'une caution non rendue
    """
    i = 1
    for e in Eleves.objects.all():
        cnr = cautions_non_rendues(e)
        if len (cnr) > 1:
            print(i, e, cnr)
            i += 1
            
def repare():
    """
    répare la base de données vis-à-vis des cautions.
    Quand un élève a plus d'une caution non rendue, la plus récente est
    supprimée et le numéro de caution de l'élève pointe sur la plus ancienne
    non rendue
    """
    i = 1
    for e in Eleves.objects.all():
        cnr = cautions_non_rendues(e)
        if len (cnr) > 1:
            for c in cnr[1:]:
                print("Caution supprimée :", c.numero)
                c.delete()
            e.no_caution = cnr[0].numero
            e.save()
            print(i, e.NomPrenomEleve, "->", e.no_caution)
            i += 1
    
